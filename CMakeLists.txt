# SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
# SPDX-License-Identifier: BSD-3-Clause

cmake_minimum_required(VERSION 3.16)

project(tinynews)

set(PROJECT_VERSION "0.9")
#set(PROJECT_VERSION_MAJOR 5)

# be c++17 compliant
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

include(FeatureSummary)

set(KF5_MIN_VERSION "5.75.0")
set(QT_MIN_VERSION "5.15.0")

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMSetupVersion)
include(ECMGenerateHeaders)
include(ECMPoQmTools)
include(KDEClangFormat)
include(KDEGitCommitHooks)

# Find Qt modules
find_package(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS
    Core    # QCommandLineParser, QStringLiteral
    Gui
    Qml
    Quick
    QuickControls2
    Svg
    WebEngineWidgets
    WebEngine
)

find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    CoreAddons      # KAboutData
    I18n            # KLocalizedString
    WidgetsAddons   # KMessageBox
    Config
    DBusAddons
)

if (ANDROID)
    find_package(Qt5 ${QT_MIN_VERSION} REQUIRED COMPONENTS Svg)
    find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS Kirigami2)
    find_package(OpenSSL REQUIRED)
endif()

#configure_file(TinyNewsVersion.h.in TinyNewsVersion.h)
ecm_setup_version(${PROJECT_VERSION}
    VARIABLE_PREFIX TINYNEWS
    VERSION_HEADER ${CMAKE_BINARY_DIR}/src/tinynews-version.h
)

#appstreamcli make-desktop-file org.kde.tinynews.appdata.xml org.kde.tinynews.desktop
install(PROGRAMS org.kde.tinynews.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES org.kde.tinynews.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})
install(FILES tinynews.svg DESTINATION ${KDE_INSTALL_FULL_ICONDIR}/hicolor/scalable/apps)

add_subdirectory(src)

kde_configure_git_pre_commit_hook(CHECKS CLANG_FORMAT)
