# Tiny News

Tiny tiny rss and pocket client application. Made with kirigami, the qml components from
kde. 

Tiny news is a client of the "Tiny tiny rss", the free and open source web-based news feed  reader and aggregator. It has also basic getpocket.com features.

An account of any tiny tiny rss that provides an api is mandatory. 
To use the getpocket.com functionalities an account of that service it's also a requirement.


# Install

It needs the readability.js library to be downloaded (^pending to be done by cmake). 
Everything done with:

```
git clone https://gitlab.com/communia/tinynews.git
cd tinynews
cd src
npm install
cd ..
mkdir build && cd  build
cmake .. -DCMAKE_PREFIX_PATH=/usr
make
sudo make install
```

# Screenshots

On desktop:

![Desktop screenshot](screenshots/tinynews.png)

On mobile (with QT_QUICK_CONTROLS_MOBILE=true):

![Mobile screenshot](screenshots/tinynews-mobile.png)
![Mobile screenshot](screenshots/Screenshot_mobile_2.png)

On tablet:

![Mobile screenshot](screenshots/tinynews-tablet.png)

