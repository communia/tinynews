/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
import QtWebEngine 1.10
import QtWebChannel 1.15
import QtQuick.Dialogs 1.2
import org.kde.tinyNews 1.0

import "qrc:ttrss.js" as TTRSS



Kirigami.Page {
    id: webPage
    title: webviewer.title
    /** @property {string} url - The url requested to be visited **/
    property string url
    /** @property {alias} article - The article prepared to be rendered **/
    property alias article : readerBackend.article
    /** @property {string} sourceRequester - The identifier of the service requesting the url **/
    property string sourceRequester
    /** @property {string} identifier - A url identifier used by the service requesting the url **/
    property string identifier
    /** @property {boolean} readed - If the url is considerated visited or not **/
    property bool readed : false
    /** @signal readedDone - Emitted when the url is considerated read **/
    signal readedDone(string sourceRequester,int identifier, string url)

    Layout.fillWidth: true
    Layout.fillHeight: true
    padding:0
    topPadding: Kirigami.Units.gridUnit*2

    actions {
        left: Kirigami.Action {
            iconName: "view-close"
            text: i18n("Close webview")
            onTriggered: {
                approot.pageStack.layers.pop() //if using stack (layers)
                //approot.pageStack.pop() // if using columns
            }
        }
        contextualActions: [
            Kirigami.Action {
                //TODO bind to unread ttrss
                iconName: "checkbox"
                text: i18n("Read done")
                visible: sourceRequester == "TTRSS"
                checkable: true
                checked: !article.unread
                onTriggered: {
                    webPage.readedDone(webPage.sourceRequester,webPage.identifier,webPage.url)
                }
            },
            Kirigami.Action {
                id: aboutOriginalWebAction
                iconName: "edit-link"
                text: i18n("view in web")
                visible: sourceRequester == "TTRSS" && Qt.resolvedUrl(webviewer.url).substring(0,14) == "reader:article"
                enabled: webviewer.loadProgress == 100
                onTriggered: {
                    webviewer.url = article.url
                    // Todo: mark read as in viewArticleAction in TtrssItemCard
                }
            },
            Kirigami.Action {
                id: aboutReaderAction
                iconName: "view-readermode"
                text: i18n("Reader")
                visible: sourceRequester == "TTRSS" && Qt.resolvedUrl(webviewer.url).substring(0,14) != "reader:article"
                enabled: webviewer.loadProgress == 100
                checkable: true
                checked: Qt.resolvedUrl(webviewer.url).substring(0,11) == "reader:read"
                onTriggered: {
                    var url = webviewer.url
                    if (checked){
                        webviewer.url = "reader:read?url=" + webviewer.url +"&title=Title"
                    } else {
                        webviewer.goBack()
                    }
                }
            },
            Kirigami.Action {
                id: readerSettingsAction
                iconName: "view-readermode"
                text: i18n("Read options")
                visible: sourceRequester == "TTRSS" && (aboutReaderAction.checked || aboutOriginalWebAction.visible)
                checkable: true
            }
        ]
    }
    header: QQC2.ToolBar {
        visible:readerSettingsAction.checked
        contentItem: Kirigami.ActionToolBar {
            alignment: Qt.AlignHCenter
            actions: [
                Kirigami.Action {
                    id: articleFontFaceAction
                    text: i18n("Font family")
                    iconName: "font-face"
                    displayHint: 2
                    displayComponent:RowLayout{
                        Kirigami.Icon {
                            source: articleFontFaceAction.iconName
                        }
                        QQC2.ComboBox {
                            Kirigami.FormData.label: articleFontFaceAction.text
                            QQC2.ToolTip.visible: hovered
                            QQC2.ToolTip.text: articleFontFaceAction.text
                            textRole: "text"
                            valueRole: "value"
                            model: [{"text": i18n("Sans-serif"), "value": "sans_serif"},
                                {"text": i18n("Serif"), "value": "serif"}]
                            Component.onCompleted: currentIndex = SettingsManager.articleFontFace
                            onActivated: {
                                SettingsManager.articleFontFace = indexOfValue(currentValue);
                                webviewer.runJavaScript("document.body.classList.remove('sans-serif','serif')");
                                webviewer.runJavaScript("document.body.classList.add('" + TTRSS.READER_FONT[SettingsManager.articleFontFace].replace("_","-") + "')");
                                //webviewer.runJavaScript("document.body.style.setProperty('--line-height'," + value + "em)");
                            }
                        }
                    }
                },
                Kirigami.Action {
                    id: articleFontSizeAction
                    text: i18n("Font size:")
                    iconName: "font-size-up"
                    displayComponent:RowLayout{
                        Kirigami.Icon {
                            source: articleFontSizeAction.iconName
                        }
                        QQC2.SpinBox {
                            id: articleFontSizeSpinBox
                            QQC2.ToolTip.visible: hovered
                            QQC2.ToolTip.text: articleFontSizeAction.text
                            value: SettingsManager.articleFontSize
                            Kirigami.FormData.label: articleFontSizeAction.text
                            from: 6
                            to: 40
                            stepSize: 2
                            onValueModified: {
                                SettingsManager.articleFontSize = value;
                                webviewer.runJavaScript("document.body.style.setProperty('--font-size'," + value/10 + "+'em')");
                            }
                        }
                    }
                },
                Kirigami.Action {
                    id:articleContentWidthAction
                    text: i18n("Content width:")
                    iconName: "zoom-fit-width"
                    displayComponent:RowLayout{
                        Kirigami.Icon {
                            source: articleContentWidthAction.iconName
                        }
                        QQC2.SpinBox {
                            id: articleContentWidth
                            QQC2.ToolTip.visible: hovered
                            QQC2.ToolTip.text: articleContentWidthAction.text
                            value: SettingsManager.articleContentWidth
                            Kirigami.FormData.label: articleContentWidthAction.text
                            from: 20
                            to: 60
                            stepSize: 5
                            textFromValue:  function(value, locale) { return Number(value).toLocaleString(locale, 'f', 0) + "em"; }
                            onValueModified: {
                                SettingsManager.articleContentWidth = value;
                                webviewer.runJavaScript("document.body.style.setProperty('--content-width','" + value + "em')");
                            }
                        }
                    }
                },
                Kirigami.Action {
                    id: articleLineSpacingAction
                    text:  i18n("Line spacing:")
                    icon.name: "text_line_spacing"
                    displayComponent:RowLayout{
                        Kirigami.Icon {
                            source: articleLineSpacingAction.iconName
                        }
                        QQC2.SpinBox {
                            id: articleLineSpacing
                            QQC2.ToolTip.visible: hovered
                            QQC2.ToolTip.text: articleLineSpacingAction.text
                            value: SettingsManager.articleLineSpacing
                            Kirigami.FormData.label: articleLineSpacingAction.text
                            from: 10
                            to: 260
                            stepSize: 20
                            property int decimals: 1
                            property real realValue: value / 100

                            validator: DoubleValidator {
                                bottom: Math.min(articleLineSpacing.from, articleLineSpacing.to)
                                top:  Math.max(articleLineSpacing.from, articleLineSpacing.to)
                            }

                            textFromValue: function(value, locale) {
                                return Number(value / 100).toLocaleString(locale, 'f', articleLineSpacing.decimals) + "em"
                            }

                            valueFromText: function(text, locale) {
                                return Number.fromLocaleString(locale, text) * 10
                            }
                            onValueModified: {
                                SettingsManager.articleLineSpacing = value;
                                webviewer.runJavaScript("document.getElementsByClassName('container')[0].style.setProperty('--line-height', %1);".arg(SettingsManager.articleLineSpacing/100 ));
                            }
                        }
                    }
                },
                Kirigami.Action {
                    id: articleThemeAction
                    text: "Theme"
                    icon.name: "contrast"
                    displayComponent:RowLayout{
                        Kirigami.Icon {
                            source: articleThemeAction.iconName
                        }
                        QQC2.ComboBox {
                            Kirigami.FormData.label: articleThemeAction.text
                            QQC2.ToolTip.visible: hovered
                            QQC2.ToolTip.text: articleThemeAction.text
                            Layout.alignment: Qt.AlignHCenter
                            textRole: "text"
                            valueRole: "value"
                            model: [{"text": i18n("light"), "value": "light"},
                                {"text": i18n("dark"), "value": "dark"},
                                {"text": i18n("sepia"), "value": "sepia"}]
                            Component.onCompleted: currentIndex = SettingsManager.articleTheme
                            onActivated: {
                                SettingsManager.articleTheme = indexOfValue(currentValue);
                                webviewer.runJavaScript("document.body.classList.remove('sepia', 'dark', 'light')");
                                webviewer.runJavaScript("document.body.classList.add('" + TTRSS.READER_THEMES[SettingsManager.articleTheme] + "')");
                            }
                        }
                    }
                },
                Kirigami.Action {
                    id: applyReaderSettings
                    iconName: "apply"
                    text: i18n("apply")
                    visible: false
                    onTriggered: {
                        Qt.callLater(readerBackend.applySettings, {
                                         fontFace: TTRSS.READER_FONT[SettingsManager.articleFontFace].replace("_","-"),
                                         fontSize: SettingsManager.articleFontSize,
                                         contentWidth: SettingsManager.articleContentWidth + "em",
                                         lineSpacing:SettingsManager.articleLineSpacing/100 + "em",
                                         theme: TTRSS.READER_THEMES[SettingsManager.articleTheme]
                                     })
                    }
                }
            ]
        }
    }

    contentItem: ColumnLayout{
        WebEngineView {
            id: webviewer
            property var loadActions : []
            url: webPage.url
            //Layout.preferredWidth: 2 * parent.width / 3
            Layout.fillWidth: true
            Layout.fillHeight: true

            profile: TinyProfile
            //onUrlChanged:
            onLoadProgressChanged: {
                if (loadProgress == 100){
                    if (SettingsManager.autoReadUnreaded){
                        runJavaScript("window.location == \"" + webPage.url + "\"||document.referrer == \"" + webPage.url + "\"", function(result) {
                            if (result){
                                webPage.readed = false
                                readedTimer.restart()
                            } else {
                                readedTimer.stop()
                            }
                            webPage.readed = false
                        });
                    }
                }
                // SIMPLE but less accurate logic
                //              if (url != webPage.url){
                //                  readedTimer.stop()
                //              } else {
                //                  webPage.readed = false
                //                  readedTimer.restart()
                //              }
                //              webPage.readed = false
                //              console.log("changing " + url)

            }
            onLoadingChanged: {
                if(loadRequest.status == WebEngineView.LoadSucceededStatus){
                    console.log("Page has successfully loaded")
                    webviewer.runJavaScript("webChannelListen()")
                }
            }

            userScripts: [
                WebEngineScript{
                    id: qWebChannelScript
                    sourceUrl: "qrc:/pages/assets/qwebchannel.js"
                    worldId: WebEngineScript.MainWorld
                },
                WebEngineScript{
                    id: readabilityScript
                    sourceUrl: "qrc:/node_modules/node_modules/@mozilla/readability/Readability.js"
                    injectionPoint: WebEngineScript.DocumentCreation
                    worldId: WebEngineScript.MainWorld

                },WebEngineScript{
                    id: readabilityLoadScript
                    // as in gecko-dev toolkit/components/reader/AboutReader.jsm
                    // style as in gecko-devtoolkit/themes/shared/aboutReader.css
                    // Note that body class has theme and body style has parameters
                    sourceUrl: "qrc:/pages/assets/readerPage.js"
                    worldId: WebEngineScript.MainWorld
                }
            ]
            devToolsView: SettingsManager.enableWebviewDevTools ? webviewer_devtools : null

            /**
                To interactuate with webchannel it needs two reach the point in that:
                  1- WebEngineView.loading == WebEngineView.LoadSucceededStatus
                  2- WebChannel has loaded the backend and transports is activated,
                     That's known by backend.loadStatus >= 100
            **/
            webChannel: WebChannel {
                id: webChannel
                registeredObjects: readerBackend
            }

        }
        WebEngineView {
            id: webviewer_devtools
            //Layout.preferredWidth: 2 * parent.width / 3
            Layout.fillWidth: true
            Layout.fillHeight: true
            profile: TinyProfile
            visible: SettingsManager.enableWebviewDevTools
        }
    }
    Timer {
        id: readedTimer
        interval: SettingsManager.articleMarkReadedWait
        onTriggered: {
            webPage.readed = true
            webPage.readedDone(webPage.sourceRequester,webPage.identifier,webPage.url)
        }
    }

    MessageDialog {
        id: errorDialog

        icon: StandardIcon.Critical
        standardButtons: StandardButton.Close
        title: "Chat client"

        onAccepted: {
            close();
        }
        onRejected: {
            close();
        }
    }
    QtObject{
        id: readerBackend
        WebChannel.id: "readerBackend"

        property string fontFace : TTRSS.READER_FONT[SettingsManager.articleFontFace]
        /* @property {int} loadStatus - the loading state 0 : not loaded, 100: loaded, 200: loaded and signal loaded emitted
                                        it's defined to 100 in client's js side when new QWebChannel is completely defined.
         */
        property int loadStatus: 0
        /* @property {var} article - Will store the reader variables to fill the reader template*/
        property var article

        signal applySettings(var settings);
        signal fillContent(var content);
        signal foo(string settings);
        signal loaded()
        onLoadStatusChanged: {
            console.log("load STATUS: " + loadStatus )
            if (loadStatus == 100 ){
                console.log("channel ready")
                loaded()
                applyReaderSettings.trigger()
                loadStatus = 200
            }

        }
        onLoaded:{
            // Set
            if ( Qt.resolvedUrl(webviewer.url).substring(0,7) == "reader:" ) {
                fillContent(article)
            } else {
                // if http obtain article from readability:
                webviewer.runJavaScript("toReader()", function({err,article_data}){
                    if (!err){
                        article = article_data
                    } else {
                        console.debug(err)
                        showPassiveNotification("Error: " + err)
                    }
                })
            }
        }
    }

}
