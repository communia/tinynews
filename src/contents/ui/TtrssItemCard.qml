/*
* SPDX-FileCopyrightText: (C) 2021 Claudio Cambra <claudio.cambra@gmail.com>
* 
* SPDX-LicenseRef: GPL-3.0-or-later
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
import QtGraphicalEffects 1.12

import "qrc:ttrss.js" as TTRSS
import "qrc:pocket.mjs" as Pocket
import org.kde.tinyNews 1.0


Kirigami.SwipeListItem{
    id:ttrssItemCardRoot
    property int item_id
    onClicked: {
        if (SettingsManager.preferQuickView){
            viewArticleReaderAction.trigger()
        } else {
            viewArticleAction.trigger()
        }
    }

    //NOTE: never put a Layout as contentItem as it will cause binding loops
    //SEE: https://bugreports.qt.io/browse/QTBUG-66826
    contentItem: Item {
        implicitWidth: delegateLayout.implicitWidth
        implicitHeight: delegateLayout.implicitHeight
        GridLayout {
            id: delegateLayout
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                //IMPORTANT: never put the bottom margin
            }
            columnSpacing: Kirigami.Units.largeSpacing * 2
            columns: width > Kirigami.Units.gridUnit * 20 ? 4 : 2
            Kirigami.Icon {
                            id: newIcon
                            source: "applications-graphics"
                            Layout.preferredWidth: Kirigami.Units.iconSizes.large *3
                            Layout.preferredHeight: Kirigami.Units.iconSizes.large *2
                            clip:true
                            //Image.fillMode: Image.PreserveAspectCrop
                            visible: !flavorImage.visible
                        }
            Image {
                id: flavorImage
                source: flavor_image? flavor_image : fixImage() // has_image ? images[1].src :  "applications-graphics"
                Layout.preferredWidth: Kirigami.Units.iconSizes.large *3
                Layout.preferredHeight: Kirigami.Units.iconSizes.large *2
                //Layout.fillHeight: true


                function fixImage(){
                    if (attachments.count > 0) {
                        if ((/^image/).test(attachments.get(0).content_type)) {
                            return attachments.get(0).content_url
                        }
                    } else {
                        var img = TTRSS.firstImg(content)
                        if (img) {
                            return img
                        }
                        else{ return "" }//"xapplications-graphics" }
                    }
                }
                visible: source != ""
                clip:true
                fillMode: Image.PreserveAspectCrop
                property bool rounded: true
                property bool adapt: true

                layer.enabled: rounded
                layer.effect: OpacityMask {
                    maskSource: Item {
                        width: flavorImage.width
                        height: flavorImage.height
                        Rectangle {
                            anchors.centerIn: parent
                            width: flavorImage.adapt ? flavorImage.width : Math.min(flavorImage.width, flavorImage.height)
                            height: flavorImage.adapt ? flavorImage.height : width
                            radius: 4
                        }
                    }
                }

            }


            ColumnLayout {
                spacing: Kirigami.Units.smallSpacing
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignVCenter
                RowLayout{
                    spacing: Kirigami.Units.largeSpacing
                    Kirigami.Icon {
                        Layout.maximumHeight: playedLabel.implicitHeight
                        Layout.maximumWidth:  playedLabel.implicitHeight
                        source: "checkbox"
                        visible: !unread
                    }
                    QQC2.Label {
                        id: playedLabel
                        text: i18n("Read") +  "  ·"
                        font: Kirigami.Theme.smallFont
                        visible: !unread
                        opacity: 0.7
                    }
                    QQC2.Label {
                        id: feedTitleLabel
                        text: feed_title +  "  ·"
                        font: Kirigami.Theme.smallFont
                        opacity: 0.7
                    }
                    QQC2.Label {
                        Layout.fillWidth: true
                        wrapMode: Text.WordWrap
                        font: Kirigami.Theme.smallFont
                        opacity: 0.7
                        text: {
                            var time = new Date(0)
                            time.setUTCSeconds(updated)
                            return time.toLocaleDateString(Qt.locale(), Locale.NarrowFormat)
                        }
                    }
                }

                Kirigami.Heading {
                    id: heading
                    level: 3
                    text: title
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                    wrapMode: "WordWrap"
                    Layout.fillHeight: true
                    font.weight: unread?Font.Bold:Font.Normal
                    opacity: unread?1:0.5

                }


                QQC2.Label {
                    id: teaser
                    text: excerpt.trim()
                    baseUrl: link
                    textFormat: Text.StyledText
                    wrapMode: Text.WordWrap // Not Working with RichText but with StyledText
                    font.weight: Font.Light
                    elide: Text.ElideRight // Not Working with RichText but with StyledText
                    verticalAlignment: Text.AlignVCenter
                    Layout.fillWidth: true
                    font.pointSize: Kirigami.Units.fontMetrics.font.pointSize
                    maximumLineCount: 3 // Not Working with RichText but with StyledText
                    topPadding: Kirigami.Units.smallSpacing
                    bottomPadding: Kirigami.Units.largeSpacing*2
                    opacity:unread?1:0.5
                }
        }
    }
    }

    actions: [
        Kirigami.Action {
            text: i18n("Star")
            icon.name: marked? "rating" : "rating-unrated"
            onTriggered: {
                TTRSS.updateArticle(id, marked?0:1, 0 , "", function(err,data){
                    if (err){
                        console.error(err)
                        showPassiveNotification("Error: " + err)
                    } else {
                        marked = !marked
                        showPassiveNotification(i18n("%1 starred", title))
                    }
                })
            }
        },
        Kirigami.Action {
            text: i18n("Pocket it!")
            icon.name: "download-later"
            onTriggered: Pocket.add(SettingsManager.pocketConsumerKey ,SettingsManager.pocketAccessToken,{url: link},function(err,data){
                if (err){
                    console.error(err)
                    showPassiveNotification("Error: " + err)
                } else {
                    showPassiveNotification(i18n("%1 saved to pocket", title))
                }
            })
            visible: SettingsManager.pocketConsumerKey
        },
        Kirigami.Action {
            id: viewArticleReaderAction
            text: i18n("Quick view")
            icon.name: "quickview"
            onTriggered: {
                // look at TTRSSPage.qml : fetch function to inspect properties
                let webPageStack = approot.pageStack.layers.push("qrc:/WebPage.qml", {
                                                                     url : "reader:article?url=" + link,
                                                                     sourceRequester:"TTRSS",
                                                                     identifier: id,
                                                                     article: {
                                                                         content: content,
                                                                         siteName: feed_title,
                                                                         title: title,
                                                                         byline: author,
                                                                         length:content.length,
                                                                         unread: unread,
                                                                         url: link
                                                                     }
                                                                 }) //if using stack (layers)
                webPageStack.readedDone.connect(markReaded)

                //approot.pushPage("WebPage", {url : "https://getpocket.com/read/" + item_id }) //if using columns
            }
            function markReaded(sourceRequester,identifier, url){
                TTRSS.updateArticle(id, 0, 2 , "", function(err,data){
                    if (err){
                        console.error(err)
                        showPassiveNotification("Error: " + err)
                    } else {
                        unread = false
                        console.debug("item "+ id + " marked as read")
                        showPassiveNotification(i18n("%1 marked read", title))
                    }
                })
            }
        },
        Kirigami.Action {
            id: viewArticleAction
            text: i18n("External browse")
            icon.name: "link"
            onTriggered: {
                console.debug( id + " clicked")
                let webPageStack = approot.pageStack.layers.push("qrc:/WebPage.qml", {url : link, sourceRequester:"TTRSS", identifier: id}) //if using stack (layers)
                webPageStack.readedDone.connect(markReaded)

                //approot.pushPage("WebPage", {url : "https://getpocket.com/read/" + item_id }) //if using columns
            }
            visible: true
            function markReaded(sourceRequester,identifier, url){
                // mark readed via ttrss api
                TTRSS.updateArticle(id, 0, 2 , "", function(err,data){
                    if (err){
                        console.error(err)
                        showPassiveNotification("Error: " + err)
                    } else {
                        unread = false
                        console.debug("item "+ id + " marked as read")
                        showPassiveNotification(i18n("%1 marked read", title))
                    }
                })
            }
        }


    ]
}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
