/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import Qt.labs.platform 1.1
import QtQuick.Layouts 1.14
import QtWebEngine 1.10
import QtWebChannel 1.15
import QtWebSockets 1.15
import QtQuick.Window 2.2

import org.kde.kirigami 2.14 as Kirigami

import org.kde.tinyNews 1.0

import "qrc:/pocket.mjs" as Pocket


Kirigami.ScrollablePage {
    title: i18n("Settings")
    property string pocketTemporalCode

    Kirigami.FormLayout {

        Kirigami.Separator {
            Kirigami.FormData.label: "Pocket Account"
            Kirigami.FormData.isSection: true
        }
        QQC2.TextField {
            id: pocketConsumerKey
            text: SettingsManager.pocketConsumerKey
            Kirigami.FormData.label: "Pocket consumer key:"
            readOnly:true
        }

        QQC2.TextField {
            id: pocketAccesToken
            Kirigami.FormData.label: "Pocket access token:"
            text: SettingsManager.pocketAccessToken
            readOnly:true
        }

        QQC2.TextField {
            id: pocketUsername
            Kirigami.FormData.label: "Pocket username:"
            text: SettingsManager.pocketUsername
            readOnly:true
        }

        QQC2.Button {
            text: i18n("Sign in")
            onClicked: {
                // STEP 1 An app must be created manually in getpocket if consumerkey gives error in next steps.
                // STEP 2 : Request Token
                Pocket.getRequestToken(SettingsManager.pocketConsumerKey,  "pocket://logged" , {}, function(err, data){
                    if (err) {
                        console.error(i18n("Could not get request token from pocket") + ":" + i18n(err));
                    } else {
                        console.debug(i18n("Got the code") + ":" + JSON.stringify(data));
                        pocketTemporalCode = data.code
                        // STEP 3 Authorize
                        webviewer.url = "https://getpocket.com/auth/authorize.php?request_token=" + data.code + "&redirect_uri=" +  "pocket://logged"
                        webWin.show()
                        console.debug(webviewer.url + " Will redirect to STEP 4 Receive Callback from pocket.")
                    }
                })
            }
        }
        QQC2.Dialog {
            id:webView
            title: qsTr("About")

            QQC2.Label {
                text: "Lorem ipsum..."
            }


            footer:Rectangle{ width:100;  height: 750
                WebEngineView {
                    id: webview
                    url: "https://www.qt.io"
                    Layout.preferredWidth: 2 * parent.width / 3
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    onUrlChanged: {
                    }
                }
            }

        }
        Window {
            id:webWin
            x: 100; y: 100; width: 750; height: 750
            WebEngineView {
                id: webviewer
                anchors.fill: parent
                url: "pocket://about"
                Layout.preferredWidth: 2 * parent.width / 3
                Layout.fillWidth: true
                Layout.fillHeight: true
                profile: TinyProfile
                onUrlChanged: {
                    if (url == "pocket://logged") {
                        console.debug("triggered accesToken step 5")
                        // STEP 5 Convert a request token into a Pocket access token
                        Pocket.getAccessToken(SettingsManager.pocketConsumerKey, pocketTemporalCode, function(err, data){
                            if (err) {
                                console.error(i18n("Could not get access token from pocket") + ":" + i18n(err));
                            } else {
                                console.debug(i18n("Got the access token code") + ":" + JSON.stringify(data));
                                SettingsManager.pocketAccessToken = data.access_token
                                SettingsManager.pocketUsername = data.username
                                pocketAccesToken.text = SettingsManager.pocketAccessToken
                                pocketUsername.text = SettingsManager.pocketUsername
                                // STEP FINAL Show the succesfully authorization
                                webWin.show()
                                webviewer.url = "pocket://authorized"
                            }
                        })
                    }
                    if (url == "pocket://quit") {
                        console.debug("quiting")
                        webWin.close()
                    }
                }
                settings.unknownUrlSchemePolicy: WebEngineSettings.AllowAllUnknownUrlSchemes
            }
        }

        QQC2.DelayButton{
            id:delayReset
            text: i18n("disconnect pocket account")
            delay: 10000
            onActivated: {
                pocketUsername.text = ""
                pocketAccesToken.text = ""
                pocketConsumerKey.text = ""
                text = i18n("account credential cleaned")
            }

            transition: Transition {
                NumberAnimation {
                    duration: delayReset.delay * (delayReset.pressed ? 1.0 - delayReset.progress : 0.3 * delayReset.progress)
                }
            }

        }

        Kirigami.Separator {
            Kirigami.FormData.label: "TTRSS Account"
            Kirigami.FormData.isSection: true
        }

        QQC2.TextField {
            id: ttrssUrl
            Kirigami.FormData.label: "TTRSS url:"
            text: SettingsManager.ttrssUrl
            onEditingFinished: {
                SettingsManager.ttrssUrl = ttrssUrl.text
                SettingsManager.ttrssSid = "undefined"
            }
        }

        QQC2.TextField {
            id: ttrssUsername
            Kirigami.FormData.label: "TTRSS username:"
            text: SettingsManager.ttrssUsername
            onEditingFinished: SettingsManager.ttrssUsername = ttrssUsername.text
        }

        QQC2.TextField {
            id: ttrssPassword
            Kirigami.FormData.label: "TTRSS password:"
            text: SettingsManager.ttrssPassword
            echoMode: TextInput.PasswordEchoOnEdit
            onEditingFinished: SettingsManager.ttrssPassword = ttrssPassword.text
        }

        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("TTRss behavior")
        }

        QQC2.CheckBox {
            id: autoReadUnreaded
            checked: SettingsManager.autoReadUnreaded
            text: i18n("Automatically mark as read when viewing articles")
            onToggled: SettingsManager.autoReadUnreaded = checked
        }

        QQC2.CheckBox {
            id: preferQuickView
            checked: SettingsManager.preferQuickView
            text: i18n("Prefer quick view rather than web view")
            onToggled: SettingsManager.preferQuickView = checked
        }

        QQC2.SpinBox {
            id: articleMarkReadedWait
            visible: autoReadUnreaded.checked
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Seconds waited to mark as read:")
            value: SettingsManager.articleMarkReadedWait/1000
            Kirigami.FormData.label: i18n("Seconds waited to mark as read:")
            from: 1
            to:600
            onValueModified: {
                SettingsManager.articleMarkReadedWait = value * 1000;
            }
        }

        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Network")
            visible: false
        }


        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Reader mode")
        }

        QQC2.ComboBox {
            Kirigami.FormData.label: i18n("Font family")
            textRole: "text"
            valueRole: "value"
            model: [{"text": i18n("Sans-serif"), "value": "sans_serif"},
                {"text": i18n("Serif"), "value": "serif"}]
            Component.onCompleted: currentIndex = SettingsManager.articleFontFace
            onActivated: {
                SettingsManager.articleFontFace = indexOfValue(currentValue);
            }
        }
        QQC2.SpinBox {
            id: articleFontSizeSpinBox
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Font size:")
            value: SettingsManager.articleFontSize
            Kirigami.FormData.label: i18n("Font size:")
            from: 6
            to: 40
            stepSize: 2
            onValueModified: {
                SettingsManager.articleFontSize = value;
            }
        }
        QQC2.SpinBox {
            id: articleContentWidth
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Content width:")
            value: SettingsManager.articleContentWidth
            Kirigami.FormData.label: i18n("Content width:")
            from: 20
            to: 60
            stepSize: 5
            textFromValue:  function(value, locale) { return Number(value).toLocaleString(locale, 'f', 0) + "em"; }
            onValueModified: {
                SettingsManager.articleContentWidth = value;
            }
        }

        QQC2.SpinBox {
            id: articleLineSpacing
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Line spacing:")
            value: SettingsManager.articleLineSpacing
            Kirigami.FormData.label: i18n("Line spacing:")
            from: 10
            to: 260
            stepSize: 20
            property int decimals: 1
            property real realValue: value / 100

            validator: DoubleValidator {
                bottom: Math.min(articleLineSpacing.from, articleLineSpacing.to)
                top:  Math.max(articleLineSpacing.from, articleLineSpacing.to)
            }

            textFromValue: function(value, locale) {
                return Number(value / 100).toLocaleString(locale, 'f', articleLineSpacing.decimals) + "em"
            }

            valueFromText: function(text, locale) {
                return Number.fromLocaleString(locale, text) * 10
            }
            onValueModified: {
                SettingsManager.articleLineSpacing = value;
            }
        }
        QQC2.ComboBox {
            Kirigami.FormData.label: i18n("Theme")
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: i18n("Theme")
            Layout.alignment: Qt.AlignHCenter
            textRole: "text"
            valueRole: "value"
            model: [{"text": i18n("light"), "value": "light"},
                {"text": i18n("dark"), "value": "dark"},
                {"text": i18n("sepia"), "value": "sepia"}]
            Component.onCompleted: currentIndex = SettingsManager.articleTheme
            onActivated: {
                SettingsManager.articleTheme = indexOfValue(currentValue);
            }
        }
        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Developer options")
        }

        QQC2.CheckBox {
            id: enableWebviewDevTools
            checked: SettingsManager.enableWebviewDevTools
            text: i18n("Enable developer tools in web viewer")
            onToggled: SettingsManager.enableWebviewDevTools = checked
        }
        Kirigami.Heading {
            Kirigami.FormData.isSection: true
            text: i18n("Information")
        }

        QQC2.Button {
            id: about
            icon.name: "help-about"
            text: i18n("About")
            onClicked: aboutPushAction.trigger()
        }
    }
}

