/*
 *    SPDX-License-Identifier: GPL-2.0-or-later
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */
import QtQml 2.1
import org.kde.kirigami 2.14 as Kirigami


Kirigami.AboutPage {
    aboutData: tinyNewsAboutData
}
