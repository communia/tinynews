/*
* SPDX-FileCopyrightText: (C) 2021 Claudio Cambra <claudio.cambra@gmail.com>
* 
* SPDX-LicenseRef: GPL-3.0-or-later
*/

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
import QtGraphicalEffects 1.12

Kirigami.SwipeListItem {
    onReleased: openPocketSheet(index)
    signal openPocketSheet(int item)
    //NOTE: never put a Layout as contentItem as it will cause binding loops
    //SEE: https://bugreports.qt.io/browse/QTBUG-66826
    contentItem: Item {
        implicitWidth: delegateLayout.implicitWidth
        implicitHeight: delegateLayout.implicitHeight
        GridLayout {
            id: delegateLayout
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                //IMPORTANT: never put the bottom margin
            }
            rowSpacing: Kirigami.Units.largeSpacing
            columnSpacing: Kirigami.Units.largeSpacing * 2
            columns: width > Kirigami.Units.gridUnit * 20 ? 4 : 2
            Kirigami.Icon {
                source: has_image ? images[1].src :  "applications-graphics"
                Layout.fillHeight: true
                Layout.maximumHeight: Kirigami.Units.iconSizes.enormous *2
                Layout.preferredWidth: height*1.5
                visible: !flavorImage.visible

            }
            Image {
                id: flavorImage
                source: has_image ? images[1].src : "" // has_image ? images[1].src :  "applications-graphics"
                Layout.preferredWidth: Kirigami.Units.iconSizes.large *3
                Layout.preferredHeight: Kirigami.Units.iconSizes.large *2
                //Layout.fillHeight: true
                visible: source != ""
                clip:true
                fillMode: Image.PreserveAspectCrop
                property bool rounded: true
                property bool adapt: true

                layer.enabled: rounded
                layer.effect: OpacityMask {
                    maskSource: Item {
                        width: flavorImage.width
                        height: flavorImage.height
                        Rectangle {
                            anchors.centerIn: parent
                            width: flavorImage.adapt ? flavorImage.width : Math.min(flavorImage.width, flavorImage.height)
                            height: flavorImage.adapt ? flavorImage.height : width
                            radius: 4
                        }
                    }
                }

            }
            ColumnLayout {
                Kirigami.Heading {
                    level: 3
                    text: resolved_title
                    elide: Text.ElideRight
                    wrapMode: "WordWrap"
                    Layout.fillHeight: true
                    font.weight: unread?Font.Bold:Font.Normal
                    opacity: unread?1:0.5
                }
                QQC2.Label {
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    opacity: 0.7
                    text: {
                        var time = new Date(0)
                        time.setUTCSeconds(time_updated)
                        return i18n("%2 · %1 min · %3", time_to_read, resolved_url, time.toLocaleDateString(Qt.locale(), Locale.NarrowFormat) )
                    }
                }

                QQC2.Label {
                    id: teaser
                    text: excerpt.trim()
                    visible: excerpt.length > 0
                    baseUrl: link
                    textFormat: Text.StyledText
                    wrapMode: Text.WordWrap // Not Working with RichText but with StyledText
                    font.weight: Font.Light
                    elide: Text.ElideRight // Not Working with RichText but with StyledText
                    verticalAlignment: Text.AlignVCenter
                    Layout.fillWidth: true
                    font.pointSize: Kirigami.Units.fontMetrics.font.pointSize
                    maximumLineCount: 3 // Not Working with RichText but with StyledText
                    topPadding: Kirigami.Units.smallSpacing
                    bottomPadding: Kirigami.Units.largeSpacing*2
                    opacity:0.5
                }
            }
        }
    }
    actions: [
        Kirigami.Action {
            id: viewArticleAction
            text: i18n("External browse")
            icon.name: "link"
            onTriggered: {
                console.debug( item_id + " clicked")
                //openPocketSheet(item_id)
                approot.pageStack.layers.push("qrc:/WebPage.qml", {url : "https://getpocket.com/read/" + item_id }) //if using stack (layers)
                //approot.pushPage("WebPage", {url : "https://getpocket.com/read/" + item_id }) //if using columns
            }
        }

    ]
}
