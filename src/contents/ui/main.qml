/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
import Qt.labs.platform 1.1 as Platform

Kirigami.ApplicationWindow {
    id: approot

    title: i18n("tinyNews")
    //controlsVisible: false
    //wideScreen: true
    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20
    //visibility: "FullScreen"

    property int counter: 0
    property string currentPage
    signal readed(string sourceRequester,int identifier, string url)

    function getPage(page) {
        switch (page) {
        case "SettingsPage": return "qrc:/SettingsPage.qml";
        case "AboutPage": return "qrc:/AboutPage.qml";
        case "TtrssPage": return "qrc:/TtrssPage.qml";
        case "PocketPage": return "qrc:/PocketPage.qml";
        case "WebPage": return "qrc:/WebPage.qml";
            //default: return "qrc:/TtrssPage.qml";
        }
    }

    /*
    TO GET PAGE in stack ( https://doc.qt.io/qt-5/qml-qtquick-controls2-stackview.html#finding-items ):
    stackView.pop(stackView.find(function(item) {
        return item.name == "order_id";
    }));
    or
    previousItem = stackView.get(myItem.StackView.index - 1));
    */


    function pushMainPage(page, props) {
        pageStack.pop()
        //pageStack.clear()
        pageStack.push(getPage(page), props)
        currentPage = page
        console.debug(page)
    }
    function pushPage(page, props) {
        pageStack.push(getPage(page), props)
        currentPage = page
        console.debug(page)
    }

    globalDrawer: CategoriesDrawer{
        id: categoriesDrawer
        title: i18n("tinyNews")
        titleIcon: "qrc:/images/tinynews.svg"
        //isMenu: !approot.isMobile
        modal: false
        collapsible: false
        collapsed:false
        Kirigami.BasicListItem {
            id: pocketButton
            action: pocketPushAction
        }
        Kirigami.BasicListItem {
            id: ttrssButton
            action: ttrssPushAction
        }
        Kirigami.BasicListItem {
            id: settingsButton
            action: settingsPushAction
        }
        Kirigami.BasicListItem {
            id: fullscreenButton
            action: fullScreenAction
        }
        Kirigami.BasicListItem {
            id: quitButton
            action: quitAction
        }
    }

    Kirigami.Action {
        id: ttrssPushAction
        text: i18n("TinyTinyRSS")
        icon.name: "feed-subscribe"
        onTriggered: {
            categoriesDrawer.actions = null
            pushMainPage("TtrssPage")
        }
    }
    Kirigami.Action {
        id: pocketPushAction
        text: i18n("Pocket")
        icon.name: "download-later"
        onTriggered: {
            categoriesDrawer.actions = null
            pushMainPage("PocketPage")
        }
    }
    Kirigami.Action {
        id: settingsPushAction
        text: i18n("Settings")
        iconName: "settings-configure"
        checked: currentPage == "SettingsPage"
        onTriggered: {
            //pushMainPage("SettingsPage")
            approot.pageStack.layers.push("qrc:/SettingsPage.qml") //if using stack (layers)
        }
    }
    Kirigami.Action {
        id: aboutPushAction
        text: i18n("About")
        iconName: "help-about-symbolic"
        checked: currentPage == "AboutPage"
        onTriggered: {
           approot.pageStack.layers.push("qrc:/AboutPage.qml")
        }
    }
    Kirigami.Action {
        id: fullScreenAction
        text: i18n("toggle fullscreen")
        iconName: "view-fullscreen"
        shortcut: "Ctrl+F"
        onTriggered:{
            approot.visibility = approot.visibility != 5 ? 5:2
        }
    }
    Kirigami.Action {
        id: quitAction
        text: i18n("Quit")
        iconName: "application-exit"
        onTriggered: Qt.quit()
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer

    }

    pageStack.initialPage: dashBoard

    Kirigami.Page {
        id: dashBoard
        // To get there:
        //pageStack.pop()
        //pageStack.clear()
        Layout.fillWidth: true

        title: i18n("Main Page")

        actions {
            main: Kirigami.Action {
                text: i18n("Plus One")
                icon.name: "list-add"
                tooltip: i18n("Add one to the counter")
                onTriggered: {
                    counter += 1
                }
            }
            left: Kirigami.Action {
                icon.name: "go-previous"
                onTriggered: {
                    print("Left action triggered")
                }
            }
            right: Kirigami.Action {
                icon.name: "go-next"
                onTriggered: {
                    print("Right action triggered")
                }
            }
            contextualActions: [
                Kirigami.Action {
                    text:"Action for buttons"
                    icon.name: "bookmarks"
                    onTriggered: print("Action 1 clicked")
                },
                Kirigami.Action {
                    text:"Action 2"
                    icon.name: "folder"
                    enabled: false
                },
                Kirigami.Action {
                    text: "Action for Sheet"
                    visible: sheet.sheetOpen
                }
            ]
        }
        Kirigami.OverlaySheet {
            id: sheet
            onSheetOpenChanged: page.actions.main.checked = sheetOpen
            QQC2.Label {
                wrapMode: Text.WordWrap
                text: "Lorem ipsum dolor sit amet"
            }
        }

        ColumnLayout {
            width: dashBoard.width

            anchors.centerIn: parent

            Kirigami.Heading {
                Layout.alignment: Qt.AlignCenter
                text: counter == 0 ? i18n("Hello, World!") : counter
            }

            QQC2.Button {
                Layout.alignment: Qt.AlignHCenter
                text: "+ 1"
                onClicked: counter += 1
            }
        }

        Component.onCompleted: {
            categoriesDrawer.actions = null
            pushMainPage("TtrssPage")
        }


    }

    Platform.SystemTrayIcon {
        id: systray
        icon.source: "qrc:/images/tinynews.png"
        visible:true
        menu: Platform.Menu{
            Platform.MenuItem {
                id: tinyMenuItem
                iconName: "rss"
                onTriggered: ttrssPushAction.trigger()
                text: i18n("&Ttrss")
                role:Platform.MenuItem.ApplicationSpecificRole
            }
            Platform.MenuItem {
                id: settingsMenuItem
                iconName: "settings-configure"
                onTriggered: settingsPushAction.trigger()
                text: i18n("&Settings")
                role:Platform.MenuItem.PreferencesRole
            }
            Platform.MenuItem {
                id: aboutMenuItem
                iconName: "help-about"
                onTriggered: aboutPushAction.trigger()
                text: i18n("&About")
                role:Platform.MenuItem.AboutRole
            }
            Platform.MenuItem {
                separator: true
            }
            Platform.MenuItem {
                iconName: "quit"
                text: i18n("&Quit")
                onTriggered: Qt.quit()
                role:Platform.MenuItem.QuitRole
            }
        }

        onActivated: {
            switch(reason){
                case Platform.SystemTrayIcon.Context:
                    systray.menu.open()
                    break;
                default:
                    approot.show()
                    approot.raise()
                    //approot.requestActivate()
            }
        }
    }
}
