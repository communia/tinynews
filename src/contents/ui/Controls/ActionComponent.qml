/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
/**
 * An item that represents an abstract Action viewed as form component
 *
 * @inherit Kirigami.Action
 */
Item {
    id: root
    property string iconName
    property string tooltip
    RowLayout {
        Kirigami.Icon {
            source: root.iconName
            fallback: "font-facea"
        }
        QQC2.ComboBox {
        //Kirigami.FormData.label: i18n("Font face:")

        Layout.alignment: Qt.AlignHCenter
        textRole: "text"
        valueRole: "value"
        model: [{"text": i18n("Sans-serif"), "value": "sans"},
                {"text": i18n("Serif"), "value": "serif"}]
        Component.onCompleted: currentIndex = indexOfValue(SettingsManager.articleFontFace)
        onActivated: {
            SettingsManager.articleFontFace = currentValue;
            console.log(currentValue)
            //webviewer.runJavaScript("document.body.style.setProperty('--line-height'," + value + "em)");
        }
    }
    }

}
