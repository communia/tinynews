import QtQuick 2.15
import org.kde.kirigami 2.14 as Kirigami

Kirigami.Action {
    property int catId;
    text: Math.random()
    icon.name: "list-add"
    tooltip: i18n("Add one to the counter")
    onTriggered: {
        counter += 1
    }
}
