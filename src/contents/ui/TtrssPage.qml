/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */
import QtQml.Models 2.15 as Models
import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami
// TODO sort items from plasma core or selfmade
import org.kde.plasma.core 2.0
// import org.kde.tinyNews.app 1.0
import QtQuick.Dialogs 1.1

import QtGraphicalEffects 1.0
import org.kde.tinyNews 1.0

import "qrc:ttrss.js" as TTRSS

Kirigami.ScrollablePage{
    id:ttrssPage
    /* @property {var} the category object coming from an api response*/
    property var category
    /* @property {var} the feed object coming from an api response*/
    property var feed
    /* @property {bool} if the content set is a category*/
    readonly property bool isCategory : feed && feed.id.startsWith("CAT")
    /* @property {bool} if the page changes its content depending on selected leafs in a tree*/
    property bool treeNavigated: true
    /* @property {int} the id of the current viewed set (category or feed)*/
    property int contentSetId
    /* @property {string} Serialized conf to listen for grouped configuration changes events*/
    property string ttrssConfiguration: SettingsManager.ttrssUsername+SettingsManager.ttrssUrl+SettingsManager.ttrssPassword
    /* @property {int} refreshingEpoch to store the refreshing cause */
    property int refreshingEpoch

    signal requestPage(string webTitle, string webContent, string webAuthor, string webUpdated, string webSource, string webLink)
    title: "TTRSS!"
    //    actions.right: Kirigami.Action {
    //        iconName: "test"
    //        text: i18n("test")
    //    }
    supportsRefreshing: true
    actions.main: Kirigami.Action {
        id: fetchNewAction
        iconName: "view-refresh"
        text: i18n("Fetch new")
        onTriggered: {
            ttrssPage.refreshingEpoch = TTRSS.NEWER_ITEMS
            ttrssPage.refreshing = true //fetch(TTRSS.NEWER_ITEMS) //ttrssPage.refreshing = true
        }
    }
    actions.contextualActions: [

        Kirigami.Action {
            id: fetchOldAction
            iconName: "backup"
            text: i18n("Fetch older")
            onTriggered: {
                ttrssPage.refreshingEpoch = TTRSS.OLDER_ITEMS
                ttrssPage.refreshing = true
            }
        },
        Kirigami.Action {
            iconName: "structure"
            text: i18n("Refresh categories")
            onTriggered: categoriesRefreshing.visible = true
        },
        Kirigami.Action {
            iconName: "news-unsubscribe"
            text: i18n("Unsubscribe")
            visible: !isCategory
            onTriggered: unsubscribeDialog.visible = true /**/
        },
        Kirigami.Action {
            iconName: "news-subscribe"
            text: i18n("Subscribe to...")
            onTriggered: subscribeDialog.open() /**/
        },
        Kirigami.Action {
            iconName: "news-unsubscribe"
            text: i18n("Mark as read")
            onTriggered: {
                TTRSS.catchupFeed(contentSetId, isCategory, "all", function(err,data){
                    if (err){
                        console.debug(err)
                        showPassiveNotification("Error: " + err)
                    } else {
                        showPassiveNotification("Marked as read.")
                        ttrssPage.updateCounters()
                    }
                }) }
        }
    ]

    ListModel {
        id: itemsModel
        /* @property {int} since_id - To know the most new item id from the current fetch **/
        property int since_id
    }
    mainItem: ListView {
        id: view
        model: SortFilterModel {
            sourceModel: itemsModel
            sortRole: "feed_id"//"id"
        }
        delegate:TtrssItemCard {}
        footer:QQC2.ToolButton {
            anchors.horizontalCenter: parent.horizontalCenter
            action: fetchOldAction
            visible: itemsModel.count > 0
        }
    }
    Layout.fillWidth: true

    onRefreshingChanged: {
        if (refreshing) {
            fetch(refreshingEpoch)
        } else {
            refreshingEpoch = null
        }
    }

    onContentSetIdChanged:{
        console.debug("changed feed")
        fetch(TTRSS.START_ITEMS);
    }

    onTtrssConfigurationChanged: {
        console.debug("ttrss configuration changed, reloading...")
        categoriesRefreshing.visible = true
    }

    onRequestPage: {
        webLoader.title = webTitle
        webLoader.bodyContent = webContent
        webLoader.authoring = "by <b>" + webAuthor + "</b>  on <b>"
                + webSource + "</b>, dated <b>" + webUpdated + "</b>"
        webLoader.originalLink = webLink
        webLoader.source = webSource
        console.log("Requesting from fullRepresentation.qml:" + webTitle
                    + " from source:" + webLink)
        webLoader.updateContent()
        pageStack.push(webLoader)
    }

    Component.onCompleted: {
        if (SettingsManager.ttrssUrl != ""
                && SettingsManager.ttrssUsername != ""
                && SettingsManager.ttrssPassword != "") {
            categoriesRefreshing.visible = true
        } else {
            console.log(i18n("Some parameters not found so please fill it in configuration dialog"))
            return
        }
    }

    /**
      * Will Request the refresh of items populating current itemsModel
      *
      * @param {string} operation: valid values are:
      *   - TTRSS.NEWER_ITEMS: Will get newer items than the newer item of the model,
      *       the view will make them prepended to current items
      *   - TTRSS.START_ITEMS: Will get set of items from now replacing the current items in the model
      *   - TTRSS.OLDER_ITEMS: Will get older items appending to current items in the model
      */
    function fetch(items_epoch = TTRSS.START_ITEMS){
        title = feed.name
        let is_cat = isCategory ? 1 : 0
        title = isCategory ? category.name : category.name + " > " + feed.name
        let skip = items_epoch == TTRSS.OLDER_ITEMS ? itemsModel.count : null
        let since_id = items_epoch == TTRSS.NEWER_ITEMS ? itemsModel.since_id : null
        TTRSS.getHeadlinesToPopulate(is_cat, contentSetId,since_id, skip, function(err,data){
            if (err){
                console.error(err)
                showPassiveNotification("Error: " + err)
                visible = false
                refreshing = false
            } else {
                console.debug(JSON.stringify(data))
                if (items_epoch == TTRSS.START_ITEMS){
                    itemsModel.clear()
                }
                var first_id;
                for (var key in data.content){
                    if (!first_id && items_epoch != TTRSS.OLDER_ITEMS) {
                        first_id = true
                        itemsModel.since_id = data.content[key].id
                    }
                    itemsModel.append(data.content[key])
                }
                showPassiveNotification(i18n("Fetched %1 items", data.content.length))
                //systray.showMessage("Tiny news", i18n("Fetched %1 items", data.content.length), systray.icon, 5000)
            }
            refreshing = false
        })
    }

    /**
     * Updates the counters of feeds and categories.
     */
    function updateCounters(){
        TTRSS.getCounters("flc", function(err,data){
            if (err){
                console.debug(err)
                showPassiveNotification("Error: " + err)
            } else {
                for (var count_id in data.content ){
                    categoriesDrawer.updateDrawerActionByCatId( categoriesDrawer.actions, data.content[count_id].id, function(actionItem, err){
                        if (actionItem){
                            actionItem.tooltip = data.content[count_id].counter
                            actionItem.text = actionItem.category.name + " (" + data.content[count_id].counter + ")"
                        }
                    })
                }
                showPassiveNotification("Categories counters Updated.")
            }
        })
    }

    Kirigami.Heading{
        id: ttrssConnect
        text: i18n("Configuration empty, please fill in settings")
        anchors.horizontalCenter: parent.horizontalCenter
        visible: ttrssConfiguration == ""
        level: 4
    }
    QQC2.Button {
        anchors.centerIn: parent
        text: i18n("Configure...")
        visible: ttrssConfiguration == ""
        onClicked: {
            settingsPushAction.trigger();
        }
    }

    QQC2.BusyIndicator {
        id: categoriesRefreshing
        Layout.maximumHeight: Kirigami.Units.iconSizes.large
        anchors.horizontalCenter: parent.horizontalCenter
        visible: false
        running: visible
        onVisibleChanged: {
            if (visible){
                TTRSS.getFeedTree(true, function(err,data){
                    if (err){
                        console.debug(err)
                        showPassiveNotification("Error: " + err)
                        visible = false
                    } else {
                        categoriesDrawer.actions = categoriesDrawer.createCategoryActions(data.content.categories.items)
                        showPassiveNotification("Categories Updated.")
                        ttrssPage.updateCounters()
                        visible = false
                        if (!contentSetId){
                            //Position to the first category in categoriesDrawer when no content set id is there
                            for (var cati in categoriesDrawer.actions){
                                categoriesDrawer.actions[cati].trigger()
                                break
                            }
                        }
                    }
                })
            }
        }
    }


    MessageDialog {
        id: unsubscribeDialog
        title: i18n("Unsubscribe?")
        icon: StandardIcon.Question
        text: i18n("Remove this %1 feed subscription?", ttrssPage.title)
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: TTRSS.unsubscribeFeed(contentSetId, function(err,data){
            if (err){
                console.error(err)
                showPassiveNotification("Error: " + err)
            } else {
                showPassiveNotification(i18n("%1 unsubscribed", ttrssPage.title))
            }
            visible=false
        })

    }
    Kirigami.OverlaySheet {
        id: subscribeDialog
        header: Kirigami.Heading {
            text: i18n("Subscribe?")
        }
        footer: Kirigami.FormLayout {

            QQC2.TextField {
                id:urlToSubscribe
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Url:")
            }

            QQC2.Button {
                text: i18n("Subscribe")
                Layout.fillWidth: true
                onClicked: TTRSS.subscribeToFeed(urlToSubscribe.text, categoriesCombo.currentValue, undefined, function(err,data){
                    if (err){
                        console.error(err)
                        showPassiveNotification("Error: " + err)
                    } else {
                        if (data.content.status.code === 4 ){
                            foundFeedsCombo.foundFeeds = []
                            for(var i in data.content.status.feeds) {
                                foundFeedsCombo.foundFeeds.push({value: i, text: data.content.status.feeds[i]})
                            }
                            foundFeedsCombo.model = foundFeedsCombo.foundFeeds
                            foundFeedsCombo.visible = true;

                        } else if (data.content.status.code === 1 ){
                            showPassiveNotification(i18n("%1 subscribed", urlToSubscribe.text))
                            categoriesRefreshing.visible = true
                            subscribeDialog.close()
                        }
                        else {
                            var error_reason
                            switch (data.content.status.code){
                            case 2:
                                error_reason = i18n("Specified URL seems to be invalid.")
                                break;
                            case 3:
                                error_reason = i18n("Specified URL doesn't seem to contain any feeds.")
                                break;
                            case 5:
                                error_reason = i18n("Couldn't download the specified URL.")
                                break;
                            case 0:
                                error_reason = i18n("You are already subscribed to this feed.")
                                break;
                            }

                            showPassiveNotification(i18n("Subscription to %1 error: %2", urlToSubscribe.text, error_reason))
                        }
                    }
                })
            }

        }
        Kirigami.FormLayout {
            QQC2.ComboBox {
                property var categories : []
                id: categoriesCombo
                textRole: "title"
                valueRole: "id"
                Kirigami.FormData.label: i18n("Category")
                onActivated: console.log(currentValue)

            }
            QQC2.ComboBox {
                property var foundFeeds : []
                id: foundFeedsCombo
                visible: false
                textRole: "text"
                valueRole: "value"
                Kirigami.FormData.label: i18n("Found feeds")
                onActivated: urlToSubscribe.text = currentValue
            }
        }
        onSheetOpenChanged: { foundFeedsCombo.visible = false
            sheetOpen && TTRSS.getCategories(false, false, true, function(err,data){
                if (err){
                    console.error(err)
                    showPassiveNotification("Error: " + err)
                } else {
                    showPassiveNotification(i18n("Categories updated", title))
                    categoriesCombo.categories = []
                    for(var i in data.content) {
                        if (data.content[i].id > -1){
                            categoriesCombo.categories.push(data.content[i])
                        }
                    }
                    categoriesCombo.model = categoriesCombo.categories
                }
            })
        }
    }

}
