/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */

import QtQuick 2.15
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.14 as Kirigami

import QtGraphicalEffects 1.0
import QtQuick.Controls 2.15 as QQC2
import org.kde.plasma.core 2.0

import "qrc:/pocket.mjs" as Pocket
import org.kde.tinyNews 1.0

Kirigami.ScrollablePage{
    id:pocketPage
    property int countLoading: 0
    property int lastUpdate;

    supportsRefreshing: true
    onRefreshingChanged: {
        if(refreshing) {
            itemsModel.refreshItems()
        }
    }


    signal requestPage(string webTitle, string webContent, string webAuthor, string webUpdated, string webSource, string webLink)

    Component.onCompleted: {
        categoriesDrawer.actions = drawerBench.actions
        itemsModel.refreshItems()
    }

    onRequestPage: {
        webLoader.title = webTitle
        webLoader.bodyContent = webContent
        webLoader.authoring = "by <b>" + webAuthor + "</b>  on <b>"
                + webSource + "</b>, dated <b>" + webUpdated + "</b>"
        webLoader.originalLink = webLink
        webLoader.source = webSource
        console.log("Requesting from fullRepresentation.qml:" + webTitle
                    + " from source:" + webLink)
        webLoader.updateContent()
        pageStack.push(webLoader)
    }

    Kirigami.Heading{
        id: ttrssConnect
        text: i18n("Configuration empty, please fill in settings")
        anchors.horizontalCenter: parent.horizontalCenter
        visible: !SettingsManager.pocketAccessToken
        level: 4
    }
    QQC2.Button {
        anchors.centerIn: parent
        text: i18n("Configure...")
        visible: !SettingsManager.pocketAccessToken
        onClicked: {
            root.action("configure").trigger();
        }
    }

    contextualActions: tagsDrawer.actions

    ListModel {
        id: itemsModel
        // to define the last time that fetch articles is done
        property int since: 0
        function refreshItems() {
            console.debug("refreshing since " + since )
            // to prevent api limits when developing
            //        var testItem = {"item_id":"3420960296","resolved_id":"3420960296","given_url":"https://es.communia.blog/el-ocaso-de-la-cultura-americana/","given_title":"","favorite":"0","status":"0","time_added":"1630491300","time_updated":"1630491302","time_read":"0","time_favorited":"0","sort_id":0,"resolved_title":"El ocaso de la cultura americana","resolved_url":"https://es.communia.blog/el-ocaso-de-la-cultura-americana/","excerpt":"Una crisis de la civilización como la del capitalismo se expresa necesariamente a través de la cultura: desde la desaparición del Arte en su sentido estricto a la cotidianidad del derrotismo y el vacío vital que reflejan todos los grandes productos culturales de nuestra época.","is_article":"1","is_index":"0","has_video":"0","has_image":"1","word_count":"951","lang":"es","time_to_read":4,"top_image_url":"https://es.communia.blog/wp-admin/admin-ajax.php?action=rank_math_overlay_thumb&id=27220&type=emancipacion","tags":{"economia":{"item_id":"3420960296","tag":"economia"},"emancipacion":{"item_id":"3420960296","tag":"emancipacion"},"politica":{"item_id":"3420960296","tag":"politica"}},"image":{"item_id":"3420960296","src":"https://es.communia.blog/files/2018/02/ense%C3%B1ando-chino-en-africa.jpg","width":"900","height":"684"},"images":{"1":{"item_id":"3420960296","image_id":"1","src":"https://es.communia.blog/files/2018/02/ense%C3%B1ando-chino-en-africa.jpg","width":"900","height":"684","credit":"","caption":"Enseñando chino como segunda lengua en Africa. La pérdida de la hegemonía lingüística en regiones enteras del globo es uno de los primeros signos del ocaso de la cultura americana."},"2":{"item_id":"3420960296","image_id":"2","src":"https://es.communia.blog/files/2021/08/the-chair.jpg","width":"700","height":"450","credit":"«La directora»","caption":"Dos de las protagonistas de «The Chair»"}},"listen_duration_estimate":368}
            //        append(testItem)
            //        append(testItem)
            //        pocketPage.refreshing = false
            //        return;
            Pocket.getItems(SettingsManager.pocketConsumerKey ,SettingsManager.pocketAccessToken,{ "count": 20, offset: itemsModel.count, sort: "newest", since: since,  "detailType":"complete" }, function(err, pocket_items){
                pocketPage.refreshing = false
                since = pocket_items.since

                // todo replace for a fixed sortorder model in view (as it needs to reverse collection with js here:)
                Object.keys(pocket_items.list).reverse().forEach(function(key) {
                  append(pocket_items.list[key])
                     console.debug(JSON.stringify(pocket_items.list[key]))
                })
            })

        }
        function fetchOldItems() {
            console.debug("fetch old")
            // to prevent api limits when developing
            //        var testItem = {"item_id":"3420960296","resolved_id":"3420960296","given_url":"https://es.communia.blog/el-ocaso-de-la-cultura-americana/","given_title":"","favorite":"0","status":"0","time_added":"1630491300","time_updated":"1630491302","time_read":"0","time_favorited":"0","sort_id":0,"resolved_title":"El ocaso de la cultura americana","resolved_url":"https://es.communia.blog/el-ocaso-de-la-cultura-americana/","excerpt":"Una crisis de la civilización como la del capitalismo se expresa necesariamente a través de la cultura: desde la desaparición del Arte en su sentido estricto a la cotidianidad del derrotismo y el vacío vital que reflejan todos los grandes productos culturales de nuestra época.","is_article":"1","is_index":"0","has_video":"0","has_image":"1","word_count":"951","lang":"es","time_to_read":4,"top_image_url":"https://es.communia.blog/wp-admin/admin-ajax.php?action=rank_math_overlay_thumb&id=27220&type=emancipacion","tags":{"economia":{"item_id":"3420960296","tag":"economia"},"emancipacion":{"item_id":"3420960296","tag":"emancipacion"},"politica":{"item_id":"3420960296","tag":"politica"}},"image":{"item_id":"3420960296","src":"https://es.communia.blog/files/2018/02/ense%C3%B1ando-chino-en-africa.jpg","width":"900","height":"684"},"images":{"1":{"item_id":"3420960296","image_id":"1","src":"https://es.communia.blog/files/2018/02/ense%C3%B1ando-chino-en-africa.jpg","width":"900","height":"684","credit":"","caption":"Enseñando chino como segunda lengua en Africa. La pérdida de la hegemonía lingüística en regiones enteras del globo es uno de los primeros signos del ocaso de la cultura americana."},"2":{"item_id":"3420960296","image_id":"2","src":"https://es.communia.blog/files/2021/08/the-chair.jpg","width":"700","height":"450","credit":"«La directora»","caption":"Dos de las protagonistas de «The Chair»"}},"listen_duration_estimate":368}
            //        append(testItem)
            //        append(testItem)
            //        pocketPage.refreshing = false
            //        return;
            Pocket.getItems(SettingsManager.pocketConsumerKey ,SettingsManager.pocketAccessToken,{ "count": 20, offset: itemsModel.count,  sort: "newest",  "detailType":"complete" }, function(err, pocket_items){
                pocketPage.refreshing = false
                Object.keys(pocket_items.list).reverse().forEach(function(key) {
                  append(pocket_items.list[key])
                     console.debug(JSON.stringify(pocket_items.list[key]))
                })
                fetchOldAction.text= i18n("Fetch older")

            })
        }
    }

    background: Rectangle {
        anchors.fill: parent
        Kirigami.Theme.colorSet: Kirigami.Theme.View
        color: Kirigami.Theme.backgroundColor
    }

    actions.main: Kirigami.Action {
        iconName: "view-refresh"
        text: i18n("Fetch new")
        onTriggered: pocketPage.refreshing = true
    }
    actions.right :Kirigami.Action {
        id: fetchOldAction
        iconName: "backup"
        text: i18n("Fetch older")
        onTriggered: {
            itemsModel.fetchOldItems()
            text = i18n("Fetching older... please wait")
        }
    }


    mainItem: ListView {
        id: view
        model: itemsModel
            /*SortFilterModel {
            sourceModel: itemsModel
            sortRole: "id"
            sortOrder: Qt.AscendingOrder
            // todo check what happens with order
        }*/
        delegate:PocketItemCard { }
    }
    footer:QQC2.ToolButton {
        anchors.horizontalCenter: parent.horizontalCenter
        action: fetchOldAction
        visible: itemsModel.count > 0
    }

    QQC2.ActionGroup {
        id: drawerBench
        actions: [
            Kirigami.Action {
                text: "Archive"
                onTriggered: console.log(text)
                //checked: root.pageStack.globalToolBar.style == Kirigami.ApplicationHeaderStyle.Auto
                //                Kirigami.Action {
                //                    text: "Favorites"
                //                    onTriggered: console.log(text)
                //                    Kirigami.Action {
                //                        text: "Favorites"
                //                        onTriggered: console.log(text)
                //                    }
                //                }
            },
            Kirigami.Action {
                text: "Favorites"
                onTriggered: console.log(text)
                enabled:false
            },
            Kirigami.Action {
                text: "Highlights"
                onTriggered: console.log(text)
            },
            Kirigami.Action {
                text: "Articles"
                onTriggered: console.log(text)
            },
            Kirigami.Action {
                text: "Videos"
                onTriggered: console.log(text)
            }

        ]

    }
}

