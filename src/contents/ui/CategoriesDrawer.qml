/*
 *   SPDX-FileCopyrightText: 2015 Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Layouts 1.14
import QtQuick.Controls 2.15
//import org.kde.discover 2.0
//import org.kde.discover.app 1.0
import org.kde.kirigami 2.14 as Kirigami
//import "navigation.js" as Navigation

Kirigami.GlobalDrawer {
    id: drawer

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    // FIXME: Dirty workaround for 385992
    width: Kirigami.Units.gridUnit * 14

    property bool wideScreen: false

    resetMenuOnTriggered: false

    property string currentSearchText //TODO

    onCurrentSubMenuChanged: {
        if (currentSubMenu)
            currentSubMenu.trigger()
        //else
        //    Navigation.openHome()
    }


    header: Kirigami.AbstractApplicationHeader {
        visible: drawer.wideScreen

        contentItem: RowLayout {
            anchors {
                left: parent.left
                leftMargin: Kirigami.Units.smallSpacing
                right: parent.right
                rightMargin: Kirigami.Units.smallSpacing
            }

            ToolButton {

                icon.name: "go-home"
                onClicked: Navigation.openHome()

                ToolTip {
                    text: i18n("Return to the Featured page")
                }
            }
        }
    }

    ColumnLayout {
        spacing: 0
        Layout.fillWidth: true

        Kirigami.Separator {
            Layout.fillWidth: true
        }


        states: [
            State {
                name: "full"
                when: drawer.wideScreen
                PropertyChanges { target: drawer; drawerOpen: true }
            },
            State {
                name: "compact"
                when: !drawer.wideScreen
                PropertyChanges { target: drawer; drawerOpen: false }
            }
        ]
    }

    Component {
        id: categoryActionComponent

        Kirigami.Action {
            // to create this action page where it works must contain: treeNavigated bool property; property var category(branches); property var feed(leaf);
            property var category
            property var feed
            readonly property bool itsMe: pageStack.currentItem.treeNavigated && (pageStack.currentItem.hasOwnProperty("category")  && (pageStack.currentItem.category === category) || pageStack.currentItem.hasOwnProperty("feed")  && (pageStack.currentItem.feed === feed))
            text: category ? category.name : ""
            tooltip: category.unread
            iconName: category.icon
            checked: itsMe
            onTriggered: {

                //Navigation.openCategory(category, currentSearchText)
                if (category.type == "category"){
                    pageStack.currentItem.category = category
                    pageStack.currentItem.feed = category
                    pageStack.currentItem.contentSetId = category.bare_id
                } else {
                    // DONT propagate to set page category property as it will be defined previously
                    // It will be useful to get metadata when we are in the feed
                    pageStack.currentItem.feed = category
                    pageStack.currentItem.contentSetId = feed.bare_id
                }
                console.debug("switching to cat " + category.name)
            }
        }
    }

    function createCategoryActions(categories) {
        var actions = []
        for(var i in categories) {
            var cat = categories[i];
            if (cat.type === "category"){
                cat.icon = transformIconName(cat.icon, "category")
                var catAction = categoryActionComponent.createObject(drawer, {category: cat, children: createCategoryActions(cat.items)});
                actions.push(catAction)
            } else {
                cat.icon = cat.hasOwnProperty("icon") ? transformIconName(cat.icon, "rss") : "rss"
                var feedAction = categoryActionComponent.createObject(drawer, {category: cat,  feed: cat});
                actions.push(feedAction)
            }
        }
        return actions;
    }

    //actions: createCategoryActions({"1":{id: "1", name: "Diaris", icon: "applications-science", subcategories: [{name:"Guardian",icon:"rss"},{name:"NYT",icon:"rss"}] }})//CategoryModel.rootCategories)

    /**
     * Transform icon's name to its freedesktop counterpart
     *
     * @param {string} icon_name - the name of the icon
     * @param {string} fallback_name - the fallback name of the icon
     *
     * @return The transformed icon name or the default one.
     */
    function transformIconName(icon_name, fallback_name){
        var new_icon_name
        switch (icon_name){
            case 'inbox':
                new_icon_name = "mail-folder-inbox"
                break;
            case 'whatshot':
                new_icon_name = "view-pim-news"
                break
            case 'star':
                new_icon_name = "rating"
                break
            case 'rss_feed':
                new_icon_name = "rss"
                break
            case 'archive':
                new_icon_name = "archive-extract"
                break
            case 'restore':
                new_icon_name = "view-media-recent"
                break
            default:
                new_icon_name = fallback_name
        }
        return new_icon_name
    }

    /**
     * Tries to apply callback (action_object, error) to action with id id
     * in actions list action_list.
     *
     * @param {<Action>list} action_list - Actions list to traverse.
     * @param {int} id - category id to lookup in list
     * @param {function} callback - A callback function with action object (indicating
     *     success) and string (an optional error message).
     */
    function updateDrawerActionByCatId(action_list, id, callback){
        for (var action_id in action_list){
            if (action_list[action_id].category.bare_id == id){
                 callback(action_list[action_id], false)
            }
            if (action_list[action_id].children && action_list[action_id].children.length > 0 ){
                 updateDrawerActionByCatId(action_list[action_id].children, id, callback)
            }
        }
        callback(false, "not found")
    }

    modal: !drawer.wideScreen
    handleVisible: !drawer.wideScreen
}
