// here will be our QtObject from QML side
var readerBackend;
function webChannelListen() {
    return new QWebChannel(qt.webChannelTransport, function(channel) {
        console.log(channel)
        // all published objects are available in channel.objects under
        // the identifier set in their attached WebChannel.id property
        readerBackend = channel.objects.readerBackend;
        console.log(channel.objects)

        // connect to a signal
        readerBackend.applySettings.connect(function(settings) {
            document.body.style.setProperty('--font-size', settings.fontSize)
            document.body.style.setProperty('--content-width', settings.contentWidth)
            document.body.classList.add(settings.fontFace, settings.theme);
            document.getElementsByClassName('container')[0].style.setProperty('--line-height', settings.lineSpacing)
        });
        readerBackend.foo.connect(function(s){
            alert(JSON.stringify(s));
        })
        // connect to a signal
        readerBackend.fillContent.connect(function(article) {
            document.documentElement.getElementsByTagName('title')[0].innerHTML = article.title || "";
            document.documentElement.getElementsByClassName('reader-title')[0].innerHTML = article.title || "";
            document.documentElement.getElementsByClassName('moz-reader-content')[0].innerHTML = article.content || "";
            document.documentElement.getElementsByClassName('reader-domain')[0].innerHTML = article.siteName  || "";
            document.documentElement.getElementsByClassName('reader-domain')[1].href = article_ext.url || ""; // set footer's view in web too
            document.documentElement.getElementsByClassName('reader-domain')[0].href = article_ext.url || "";
            document.documentElement.getElementsByClassName('reader-credits')[0].innerHTML = article.byline || "";
            document.documentElement.getElementsByClassName('reader-estimated-time')[0].innerHTML = article.readTime || "";
        });

        readerBackend.loadStatus = 100
    });
}


/**
 * Extracts the readability fields from current web.
 *
 * @return {object}- with parameters boolean (indicating
 *     success) and object (with parsed article object).
 */
function toReader() {
    var documentClone = document.cloneNode(true);
    var article = new Readability(documentClone).parse();
    _assignReadTime(article)
    if (article) {
        return {err:false, article_data:article};
    } else {
        return {err:'Could not parse article'};
    }
}


/**
 * Assigns the estimated reading time range of the article to the article object.
 *
 * @param article the article object to assign the reading time estimate to.
 */
function _assignReadTime(article) {
  let lang = article.language || "en";
  const readingSpeed = _getReadingSpeedForLanguage(lang);
  const charactersPerMinuteLow = readingSpeed.cpm - readingSpeed.variance;
  const charactersPerMinuteHigh = readingSpeed.cpm + readingSpeed.variance;
  const length = article.length;

  article.readingTimeMinsSlow = Math.ceil(length / charactersPerMinuteLow);
  article.readingTimeMinsFast = Math.ceil(length / charactersPerMinuteHigh);
  article.readTime = _formatReadTime(article.readingTimeMinsSlow, article.readingTimeMinsFast)
}

function _formatReadTime(slowEstimate, fastEstimate) {
  let displayStringKey = "#1'";

  // only show one reading estimate when they are the same value
  if (slowEstimate !== fastEstimate) {
    displayStringKey = "#1'-#2'";
  }
  return displayStringKey
    .replace("#1", fastEstimate)
    .replace("#2", slowEstimate);
}

/**
   * Returns the reading speed of a selection of languages with likely variance.
   *
   * Reading speed estimated from a study done on reading speeds in various languages.
   * study can be found here: http://iovs.arvojournals.org/article.aspx?articleid=2166061
   *
   * @return object with characters per minute and variance. Defaults to English
   *         if no suitable language is found in the collection.
   */
 function _getReadingSpeedForLanguage(lang) {
    const readingSpeed = new Map([
      ["en", { cpm: 987, variance: 118 }],
      ["ar", { cpm: 612, variance: 88 }],
      ["de", { cpm: 920, variance: 86 }],
      ["es", { cpm: 1025, variance: 127 }],
      ["fi", { cpm: 1078, variance: 121 }],
      ["fr", { cpm: 998, variance: 126 }],
      ["he", { cpm: 833, variance: 130 }],
      ["it", { cpm: 950, variance: 140 }],
      ["jw", { cpm: 357, variance: 56 }],
      ["nl", { cpm: 978, variance: 143 }],
      ["pl", { cpm: 916, variance: 126 }],
      ["pt", { cpm: 913, variance: 145 }],
      ["ru", { cpm: 986, variance: 175 }],
      ["sk", { cpm: 885, variance: 145 }],
      ["sv", { cpm: 917, variance: 156 }],
      ["tr", { cpm: 1054, variance: 156 }],
      ["zh", { cpm: 255, variance: 29 }],
    ]);

    return readingSpeed.get(lang) || readingSpeed.get("en");
  }


function makeBaseDocument(callback) {
    const event = new Event('build');
    var article = new Readability(document).parse();
    document.open();
    document.write(`<html>
                   <head>
                   <title>${article.title}</title>
                   </head>
                   <body class='loaded'>
                   <div class="top-anchor"></div>
                   <div class='container' dir="${article.dir}">
                   <a class="domain reader-domain" href="${document.url}">${article.siteName}</a>
                   <div class="domain-border"></div>
                   <div class='header reader-header reader-show-element'>
                   <h1 class="reader-title">${article.title}</h1>
                   <div class="credits reader-credits">${article.byline}</div>
                   <div class="meta-data">
                   <div class="reader-estimated-time" dir="${article.dir}">placeholder:article.estimated</div>
                   </div>
                   </div>
                   <hr>
                   <div class="content">
                   <div class="moz-reader-content reader-show-element">
                   ${article.content}
                   </div>
                   </div>
                   </div>
                   </body>
                   </html>`);
    var headHTML = document.documentElement.getElementsByTagName('head')[0].innerHTML;
    headHTML += '<link rel=\"stylesheet\" href=\"qrc:/pages/assets/aboutReader.css\">';
    //document.body.style.setProperty("--font-size",20)
    document.documentElement.getElementsByTagName('head')[0].innerHTML = headHTML;
    document.close();
    window.dispatchEvent(event);
    document.onload = callback();
    return document;
}

/* TARGET:
`<html>
    <head>
      <title>${article.title}</title>
    </head>
    <body class='sans-serif loaded dark' style='--font-size: 20px; --content-width: 30em;'>
      <div class="top-anchor"></div>
                   <div id="toolbar" class="toolbar-container scrolled">
                       <div class="toolbar reader-toolbar">
                         <div class="reader-controls" articledir="${article.dir}">
                   <button onClick="initWebSocket();">Connect</button>
                           <button class="close-button button " data-telemetry-id="reader-close" aria-label="Close Reader View"><span class="hover-label">Close Reader View</span></button>
                           <ul class="dropdown style-dropdown">
                             <li>
                               <button class="dropdown-toggle button style-button" data-telemetry-id="reader-type-controls" aria-label="Type controls"><span class="hover-label">Type controls</span></button>
                             </li>
                             <li class="dropdown-popup">
                               <div class="dropdown-arrow"></div>
                               <div class="font-type-buttons radiorow"><input id="radio-itemsans-serif-button" type="radio" class="radio-button" name="font-type"><label for="radio-itemsans-serif-button" class="sans-serif-button" checked="true">Sans-serif</label><input id="radio-itemserif-button" type="radio" class="radio-button" name="font-type"><label for="radio-itemserif-button" class="serif-button">Serif</label></div>
                               <div class="font-size-buttons buttonrow">
                                 <button class="minus-button" title="Decrease Font Size"></button>
                                 <span class="font-size-value">5</span>
                                 <button class="plus-button" title="Increase Font Size">
                               </button></div>
                               <div class="content-width-buttons buttonrow">
                                  <button class="content-width-minus-button" title="Decrease Content Width"></button>
                                  <span class="content-width-value">3</span>
                                  <button class="content-width-plus-button" title="Increase Content Width">
                               </button></div>
                               <div class="line-height-buttons buttonrow">
                                   <button class="line-height-minus-button" title="Decrease Line Height"></button>
                                   <span class="line-height-value">4</span>
                                   <button class="line-height-plus-button" title="Increase Line Height">
                               </button></div>
                               <div class="color-scheme-buttons radiorow"><input id="radio-itemlight-button" type="radio" class="radio-button" name="color-scheme"><label for="radio-itemlight-button" class="light-button" checked="true" title="Color Scheme Light">Light</label><input id="radio-itemdark-button" type="radio" class="radio-button" name="color-scheme"><label for="radio-itemdark-button" class="dark-button" title="Color Scheme Dark">Dark</label><input id="radio-itemsepia-button" type="radio" class="radio-button" name="color-scheme"><label for="radio-itemsepia-button" class="sepia-button" title="Color Scheme Sepia">Sepia</label></div>
                             </li>
                           </ul>
                         <ul class="dropdown narrate-dropdown"><li><button class="dropdown-toggle button narrate-toggle" data-telemetry-id="reader-listen" aria-label="Listen" hidden=""><span class="hover-label">Listen</span></button></li><li class="dropdown-popup"><div class="narrate-row narrate-control"><button class="narrate-skip-previous" disabled="" title="Back"></button><button class="narrate-start-stop" title="Start"></button><button class="narrate-skip-next" disabled="" title="Forward"></button></div><div class="narrate-row narrate-rate"><input class="narrate-rate-input" value="0" step="5" max="100" min="-100" type="range" title="Speed"></div><div class="narrate-row narrate-voices"><div class="voiceselect voice-select"><button class="select-toggle" aria-controls="voice-options">
                         <span class="label">Voice:</span> <span class="current-voice"></span>
                       </button>
                       <div class="options" id="voice-options" role="listbox" style="max-height: 126px;"></div></div></div><div class="dropdown-arrow"></div></li></ul></div>
                       </div>
                     </div>
      <div class='container' style="--line-height: 1.6em;" dir="${article.dir}">
        <a class="domain reader-domain" href="${document.url}">${article.siteName}</a>
        <div class="domain-border"></div>
        <div class='header reader-header reader-show-element'>
          <h1 class="reader-title">${article.title}</h1>
          <div class="credits reader-credits">${article.byline}</div>
          <div class="meta-data">
            <div class="reader-estimated-time" dir="${article.dir}">placeholder:article.estimated</div>
          </div>
        </div>
        <hr>
        <div class="content">
          <div class="moz-reader-content reader-show-element">
            ${article.content}
          </div>
        </div>
      </div>
    </body>
    </html>
*/
//var baseDocument = makeBaseDocument();

function makeDocument() {
    // Instead of literal templates do it with procedure
    var new_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html', null);
    var new_body = document.createElementNS('http://www.w3.org/1999/xhtml', 'body');
    new_body.setAttribute('class', 'sans-serif loaded dark');
    new_body.setAttribute('style', '--font-size: 20px; --content-width: 30em;');


    var container = document.createElementNS('http://www.w3.org/1999/xhtml', 'div');

    new_body.setAttribute('class', 'container');
    new_body.appendChild(container)

    new_doc.documentElement.appendChild(new_body);

    let destDocument = document.getElementsByTagName("html")[0];
    document.replaceChild(
                document.importNode(new_doc.documentElement, true),
                document.documentElement
                );
}





function initWebSocket() {
    try {
        if (typeof MozWebSocket == 'function')
            WebSocket = MozWebSocket;
        if ( websocket && websocket.readyState == 1 )
            websocket.close();
        var wsUri = "ws://localhost";
        websocket = new WebSocket( wsUri );
        websocket.onopen = function (evt) {
            debug("CONNECTED");
            document.getElementById("disconnectButton").disabled = false;
            document.getElementById("sendButton").disabled = false;
        };
        websocket.onclose = function (evt) {
            debug("DISCONNECTED");
            document.getElementById("disconnectButton").disabled = true;
            document.getElementById("sendButton").disabled = true;
        };
        websocket.onmessage = function (evt) {
            console.log( "Message received :", evt.data );
            debug( evt.data );
        };
        websocket.onerror = function (evt) {
            debug('ERROR: ' + evt.data);
        };
    } catch (exception) {
        debug('ERROR: ' + exception);
    }
}
