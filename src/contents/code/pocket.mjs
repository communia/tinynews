/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */
import * as Requests from "Requests.mjs";

/*
* Get request token as https://getpocket.com/developer/docs/authentication
*/
export function getRequestToken(consumer_key, redirect_uri, state, callback) {
  Requests.postJSON({
      url: "https://getpocket.com/v3/oauth/request.php",
      data: {
          consumer_key: consumer_key,
          redirect_uri: redirect_uri,
          state: JSON.stringify(state)
      }
  }, function(err, data, xhr){
			if (xhr.status == 400 && err) {
				// callback(null, data) // if we need to ignore it.
                // 404 Not Found
				var prettyErr = "Missing consumer key or redirect url."
				callback(prettyErr, data)
			} else if (xhr.status == 403 && err) {
				// 404 Not Found
				var prettyErr = "Invalid consumer key."
				callback(prettyErr, data)
			} else if (xhr.status | 0 == 50 && err) {
				// 404 Not Found
				var prettyErr = "Pocket servers error."
				callback(prettyErr, data)
			} else { // Okay response / Unknown error
				callback(err, data)
			}
  });

}

/*
* Get Access Token as https://getpocket.com/developer/docs/authentication
*/
export function getAccessToken(consumer_key, code, callback) {
  Requests.postJSON({
      url: "https://getpocket.com/v3/oauth/authorize.php",
      data: {
          consumer_key: consumer_key,
          code: code,
      }
  }, function(err, data, xhr){
            if (xhr.status == 400 && err) {
                // callback(null, data) // if we need to ignore it.
                // 404 Not Found
                var prettyErr = "Missing consumer key or redirect url or code not found."
                callback(prettyErr, data)
            } else if (xhr.status == 403 && err) {
                // 404 Not Found
                var prettyErr = "Invalid consumer key or already used code."
                callback(prettyErr, data)
            } else if (xhr.status | 0 == 50 && err) {
                // 404 Not Found
                var prettyErr = "Pocket servers error."
                callback(prettyErr, data)
            } else { // Okay response / Unknown error
                callback(err, data)
            }
  });

}

/*
* Get request token as https://getpocket.com/developer/docs/authentication
*/
export function authorize(consumer_key, redirect_uri, callback) {
    console.log("requesting token");
  Requests.postJSON({
      url: "https://getpocket.com/auth/authorize.php",
      data: {
          consumer_key: consumer_key,
          redirect_uri: redirect_uri
      }
  }, function(err, data, xhr){
			if (xhr.status == 400 && err) {
				// callback(null, data) // if we need to ignore it.
                // 404 Not Found
				var prettyErr = "Missing consumer key or redirect url."
				callback(prettyErr, data)
			} else if (xhr.status == 403 && err) {
				// 404 Not Found
				var prettyErr = "Invalid consumer key."
				callback(prettyErr, data)
			} else if (xhr.status | 0 == 50 && err) {
				// 404 Not Found
				var prettyErr = "Pocket servers error."
				callback(prettyErr, data)
			} else { // Okay response / Unknown error
				callback(err, data)
			}
  });

}
export function tester(){
    return "test ok";
}

/**
 * Gets the list of items from Pocket.
 *
 * Parameters:
 * consumer_key 	string 		Your application's Consumer Key
 * access_token 	string 		The user's Pocket access token
 * parameters  array with this optional filters/sort/operation:
 *   - favorite 	0 or 1 		See below for valid values
 *   - tag 	string 		See below for valid values
 *   - contentType 	string 		See below for valid values
 *   - sort 	string 		See below for valid values
 *   - detailType 	string 		See below for valid values
 *   - search 	string 		Only return items whose title or url contain the search string
 *   - domain 	string 		Only return items from a particular domain
 *   - since 	timestamp 		Only return items modified since the given since unix timestamp
 *   - count 	integer 		Only return count number of items
 *   - offset 	integer 		Used only with count; start returning from offset position of results
 */
export function getItems( consumer_key, access_token, parameters, callback ) {
    parameters.access_token = access_token
    parameters.consumer_key = consumer_key
    Requests.postJSON({
        url: "https://getpocket.com/v3/get.php",
        data: parameters
    }, function(err, data, xhr){
        //console.log("obtained data when getting access_token");console.log(JSON.stringify(data));console.log(JSON.stringify(err));console.log(JSON.stringify(xhr));
              if (xhr.status == 400 && err) {
                  // callback(null, data) // if we need to ignore it.
                  var prettyErr = "Invalid request, please make sure you follow the documentation for proper syntax."
                  callback(prettyErr, data)
              } else if (xhr.status == 403 && err) {
                  // 404 Not Found
                  var prettyErr = "User was authenticated, but access denied due to lack of permission or rate limiting."
                  callback(prettyErr, data)
              } else if (xhr.status == 401 && err) {
                  var prettyErr = "Problem authenticating the user."
                  callback(prettyErr, data)
              } else if (xhr.status | 0 == 50 && err) {
                  // 404 Not Found
                  var prettyErr = "Pocket servers error."
                  callback(prettyErr, data)
              } else { // Okay response / Unknown error
                  callback(err, data)
              }
    });

}

/**
 * Adds an item to the user's list.
 *
 * Parameters:
 * consumer_key 	string 		Your application's Consumer Key
 * access_token 	string 		The user's Pocket access token
 * parameters  array with this optional filters/sort/operation:
 *   - url 	string 		The URL of the item you want to save
 *   - title 	string 	optional
 *       This can be included for cases where an item does not
 *       have a title, which is typical for image or PDF URLs.
 *       If Pocket detects a title from the content of the page,
 *       this parameter will be ignored.
 *   - tags 	string 	optional
 *       A comma-separated list of tags to apply to the item
 */
export function add( consumer_key, access_token, parameters, callback ) {
    parameters.access_token = access_token
    parameters.consumer_key = consumer_key
    parameters.url = encodeURI(parameters.url)
    Requests.postJSON({
        url: "https://getpocket.com/v3/add.php",
        data: parameters
    }, function(err, data, xhr){
        //console.log("obtained data when getting access_token");console.log(JSON.stringify(data));console.log(JSON.stringify(err));console.log(JSON.stringify(xhr));
              if (xhr.status == 400 && err) {
                  // callback(null, data) // if we need to ignore it.
                  var prettyErr = "Invalid request, please make sure you follow the documentation for proper syntax."
                  callback(prettyErr, data)
              } else if (xhr.status == 403 && err) {
                  // 404 Not Found
                  var prettyErr = "User was authenticated, but access denied due to lack of permission or rate limiting."
                  callback(prettyErr, data)
              } else if (xhr.status == 401 && err) {
                  var prettyErr = "Problem authenticating the user."
                  callback(prettyErr, data)
              } else if (xhr.status | 0 == 50 && err) {
                  // 404 Not Found
                  var prettyErr = "Pocket servers error."
                  callback(prettyErr, data)
              } else { // Okay response / Unknown error
                  callback(err, data)
                  console.debug(data)
              }
    });

}

/**
 * Modifies an item in the user's list.
 *
 * Parameters:
 * consumer_key 	string 		Your application's Consumer Key
 * access_token 	string 		The user's Pocket access token
 * actions  array JSON array of actions:
 *   - url 	string 		The URL of the item you want to save
 *   - title 	string 	optional
 *       This can be included for cases where an item does not
 *       have a title, which is typical for image or PDF URLs.
 *       If Pocket detects a title from the content of the page,
 *       this parameter will be ignored.
 *   - tags 	string 	optional
 *       A comma-separated list of tags to apply to the item
 *   - action:
 *     - add:
 *       Add a new item to the user's list.
 *       Note: If you are only adding a single item, the /v3/add endpoint should be used.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - ref_id 	integer 	optional 	A Twitter status id; this is used to show tweet attribution.
 *       - tags 	string 	optional 	A comma-delimited list of one or more tags.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *       - title 	string 	optional 	The title of the item.
 *       - url 	string 	optional 	The url of the item; provide this only if you do not have an item_id.
 *     - archive:
 *       Move an item to the user's archive.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - readd:
 *       Move an item from the user's archive back into their unread list.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - favorite:
 *       Mark an item as a favorite.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - unfavorite:
 *       Remove an item from the user's favorites.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - delete:
 *       Permanently remove an item from the user's account.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tags_add:
 *       Add one or more tags to an item.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - tags 	string 		A comma-delimited list of one or more tags.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tags_remove:
 *       Remove one or more tags from an item.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - tags 	string 		A comma-delimited list of one or more tags to remove.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tags_replace:
 *       Replace all of the tags for an item with the one or more provided tags.
 *       - tags 	string 		A comma-delimited list of one or more tags to add.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tags_clear:
 *       Remove all tags from an item.
 *       - item_id 	integer 		The id of the item to perform the action on.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tag_rename:
 *       Rename a tag. This affects all items with this tag.
 *       - old_tag 	string 		The tag name that will be replaced.
 *       - new_tag 	string 		The new tag name that will be added.
 *       - time 	timestamp 	optional 	The time the action occurred.
 *     - tag_delete:
 *       Delete a tag. This affects all items with this tag.
 *       - tag 	string 		The tag name that will be deleted.
 *       - time 	timestamp 	optional 	The time the action occurred.
 */
export function send( consumer_key, access_token, actions, callback ) {
    parameters.access_token = access_token
    parameters.consumer_key = consumer_key
    parameters.url = encodeURI(parameters.url)
    Requests.postJSON({
        url: "https://getpocket.com/v3/send.php",
        data: parameters
    }, function(err, data, xhr){
        //console.log("obtained data when getting access_token");console.log(JSON.stringify(data));console.log(JSON.stringify(err));console.log(JSON.stringify(xhr));
              if (xhr.status == 400 && err) {
                  // callback(null, data) // if we need to ignore it.
                  var prettyErr = "Invalid request, please make sure you follow the documentation for proper syntax."
                  callback(prettyErr, data)
              } else if (xhr.status == 403 && err) {
                  // 404 Not Found
                  var prettyErr = "User was authenticated, but access denied due to lack of permission or rate limiting."
                  callback(prettyErr, data)
              } else if (xhr.status == 401 && err) {
                  var prettyErr = "Problem authenticating the user."
                  callback(prettyErr, data)
              } else if (xhr.status | 0 == 50 && err) {
                  // 404 Not Found
                  var prettyErr = "Pocket servers error."
                  callback(prettyErr, data)
              } else { // Okay response / Unknown error
                  callback(err, data)
                  console.debug(data)
              }
    });

}
