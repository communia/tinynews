/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */
export function request(opt, callback) {
	if (typeof opt === 'string') {
		opt = { url: opt }
	}
	var req = new XMLHttpRequest()
	req.onerror = function(e) {
		console.log('XMLHttpRequest.onerror', e)
		if (e) {
			console.log('\t', e.status, e.statusText, e.message)
			callback(e.message)
		} else {
			callback('XMLHttpRequest.onerror(undefined)')
		}
	}
	req.onreadystatechange = function() {
		if (req.readyState === XMLHttpRequest.DONE) { // https://xhr.spec.whatwg.org/#dom-xmlhttprequest-done
			if (200 <= req.status && req.status < 400) {
				callback(null, req.responseText, req)
			} else {
				if (req.status === 0) {
					console.log('HTTP 0 Headers: \n' + req.getAllResponseHeaders())
				}
				var msg = "HTTP Error " + req.status + ": " + req.statusText
				callback(msg, req.responseText, req)
			}
		}
	}
	req.open(opt.method || "GET", opt.url, true)
	if (opt.headers) {
		for (var key in opt.headers) {
			req.setRequestHeader(key, opt.headers[key])
		}
	}
    console.debug("The sent request: " + JSON.stringify(opt))
	req.send(opt.data)
}

function encodeParams(params) {
	var s = ''
	var i = 0
	for (var key in params) {
		if (i > 0) {
			s += '&'
		}
		var value = params[key]
		if (typeof value === "object") {
			// TODO: Flatten obj={list: [1, 2]} as
			// obj[list][0]=1
			// obj[list][1]=2
		}
		s += encodeURIComponent(key) + '=' + encodeURIComponent(value)
		i += 1
	}
	return s
}

function encodeFormData(opt) {
	opt.headers = opt.headers || {}
	opt.headers['Content-Type'] = 'application/x-www-form-urlencoded'
	if (opt.data) {
		opt.data = encodeParams(opt.data)
	}
	return opt
}

export function post(opt, callback) {
	if (typeof opt === 'string') {
		opt = { url: opt }
	}
	opt.method = 'POST'
	encodeFormData(opt)
	request(opt, callback)
}


export function getJSON(opt, callback) {
	if (typeof opt === 'string') {
		opt = { url: opt }
	}
	opt.headers = opt.headers || {}
	opt.headers['Accept'] = 'application/json'
	request(opt, function(err, data, req) {
		if (!err && data) {
            try {
                data = JSON.parse(data)
            } catch (e) {
                data = {}
                err = "Response is not in JSON format."
            }
		}
		callback(err, data, req)
	})
}


export function postJSON(opt, callback) {
    console.log("calling postJSON");

	if (typeof opt === 'string') {
		opt = { url: opt }
	}
	opt.method = opt.method || 'POST'
	opt.headers = opt.headers || {}
	opt.headers['Content-Type'] = 'application/json; charset=UTF-8'
    opt.headers['X-Accept'] = 'application/json'
	if (opt.data) {
		opt.data = JSON.stringify(opt.data)
	}
	getJSON(opt, callback)
}

export function getFile(url, callback) {
	var req = new XMLHttpRequest()
	req.onerror = function(e) {
		console.log('XMLHttpRequest.onerror', e)
		if (e) {
			console.log('\t', e.status, e.statusText, e.message)
			callback(e.message)
		} else {
			callback('XMLHttpRequest.onerror(undefined)')
		}
	}
	req.onreadystatechange = function() {
		if (req.readyState === 4) {
			// Since the file is local, it will have HTTP 0 Unsent.
			callback(null, req.responseText, req)
		}
	}
	req.open("GET", url, true)
	req.send()
}
