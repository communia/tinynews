/*
 *    SPDX-License-Identifier: GPL-3.0-only
 *    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
 */
.import "Requests.mjs" as Requests
.import org.kde.tinyNews 1.0 as TinyNews

// NEWER_ITEMS: Will get newer items than the newer item of the model,
// the view will make them prepended to current items.
const NEWER_ITEMS = 0
// START_ITEMS: Will get set of items from now replacing the current items in the model
const START_ITEMS = 1
// OLDER_ITEMS: Will get older items appending to current items in the model
const OLDER_ITEMS = 2

const READER_THEMES = ['light', 'dark', 'sepia']
const READER_FONT = ['sans-serif', 'serif']
/**
 * Get sid token as https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#login .
 * @param {int} seq - Arbitrary sequential that will get there in every api return
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getSid(seq, callback) {
  Requests.postJSON({
      url: TinyNews.SettingsManager.ttrssUrl + "/api/",
      data: {
          op: "login",
          user: TinyNews.SettingsManager.ttrssUsername,
          password: TinyNews.SettingsManager.ttrssPassword,
          seq: seq
      }
  }, function(err, data, xhr){
            if (xhr.status == 400 && err) {
                // callback(null, data) // if we need to ignore it.
                // 404 Not Found
                var prettyErr = "Missing consumer key or redirect url."
                callback(prettyErr, data)
            } else if (xhr.status == 403 && err) {
                // 404 Not Found
                var prettyErr = "Invalid consumer key."
                callback(prettyErr, data)
            } else if (xhr.status | 0 == 50 && err) {
                // 404 Not Found
                var prettyErr = "Pocket servers error."
                callback(prettyErr, data)
            } else if (xhr.status == 200 && data.status == 1){
                if (data.content.error == "API_DISABLED" ){
                    var prettyErr = "Api is disabled."
                    callback(prettyErr, data)
                } else if (data.content.error == "LOGIN_ERROR" ){
                    var prettyErr = "Login error."
                    callback(prettyErr, data)
                } else if (data.content.error == "INCORRECT_USAGE" ){
                    var prettyErr = "Incorrect api usage."
                    callback(prettyErr, data)
                } else if (data.content.error == "UNKNOWN_METHOD" ){
                    var prettyErr = "This method is unknown by the service."
                    callback(prettyErr, data)
                } else if (data.content.error == "E_OPERATION_FAILED" ){
                    var prettyErr = "The operation failed in service."
                    callback(prettyErr, data)
                }
            } else { // Okay response / Unknown error
                TinyNews.SettingsManager.ttrssSid = data.content.session_id
                callback(err, data)
            }
  });

}

/**
 * Base call api as https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference .
 * @param {string} op - The operation to be done.
 * @param {object} op_params - The operation parameters.
 * @param {int} seq - Arbitrary sequential that will get there in every api return
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function callApi(op, op_params, seq, callback) {
    // Post json, note the merge of op, op_params, sid and seq in a new object, with all keys in the same level
    let formed_data = op_params
    formed_data.sid = TinyNews.SettingsManager.ttrssSid
    formed_data.op = op
    formed_data.seq = seq
    Requests.postJSON({
        url: TinyNews.SettingsManager.ttrssUrl + "/api",
        data: formed_data
    }, function(err, data, xhr){
              if (xhr.status === 400 && err) {
                  // callback(null, data) // if we need to ignore it.
                  // 404 Not Found
                  var prettyErr = "Missing consumer key or redirect url."
                  callback(prettyErr, data)
              } else if (xhr.status == 403 && err) {
                  // 404 Not Found
                  var prettyErr = "Invalid consumer key."
                  callback(prettyErr, data)
              } else if (xhr.status | 0 == 50 && err) {
                  // 404 Not Found
                  var prettyErr = "Pocket servers error."
                  callback(prettyErr, data)
              } else if (xhr.status == 200 && data.status == 1){
                  if (data.content.error == "API_DISABLED" ){
                      var prettyErr = "Api is disabled."
                      callback(prettyErr, data)
                  } else if (data.content.error == "LOGIN_ERROR" ){
                      var prettyErr = "Login error."
                      callback(prettyErr, data)
                  } else if (data.content.error == "INCORRECT_USAGE" ){
                      var prettyErr = "Incorrect api usage."
                      callback(prettyErr, data)
                  } else if (data.content.error == "NOT_LOGGED_IN" ){
                      console.debug(data.content.error)

                      getSid(0, function(err, data){
                          if (err) {
                              var prettyErr = "Could not login because: " + err
                              callback(prettyErr, data)
                          } else {
                              // Recursive try again to do what we did before
                              callApi(op, op_params, 1, callback)
                          }
                      });
                  } else if (data.content.error == "UNKNOWN_METHOD" ){
                      var prettyErr = "This method is unknown by the service."
                      callback(prettyErr, data)
                  } else if (data.content.error == "E_OPERATION_FAILED" ){
                      var prettyErr = "The operation failed in service."
                      callback(prettyErr, data)
                  }
              } else { // Okay response / Unknown error
                  callback(err, data)
              }
    });
}

/**
 * Update article call api as @see https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#updatearticle .
 * @param {string} article_ids -(comma-separated list of integers) - article IDs to operate on
 * @param {int} mode - type of operation to perform:  (0 - set to false, 1 - set to true, 2 - toggle)
 * @param {int} field - field to operate on (0 - starred, 1 - published, 2 - unread, 3 - article note)
 * @param {string} param_data - optional data parameter when setting note field
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function updateArticle(article_ids, mode, field, param_data, callback) {
    callApi("updateArticle",{article_ids: article_ids, mode: mode, field: field, data: param_data }, 0,  callback)
}

/**
 * Shorthand function to obtain headlines as regular.
 *
 * @param {boolean} is_cat - True if the specified feed_id is a category
 * @param {int} feed_id - ID of feed.
 * @param {int} since_id - Only return articles with id greater than since_id
 * @param {int} skip - skip this amount of items first.
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getHeadlinesToPopulate(is_cat, feed_id, since_id, skip, callback) {
    // used also for retrieving new items (using since_id set with first headline id in the model)
    // used also for retrieving past items (using skip set to number of items counter of the model)
     callApi("getHeadlines", {"is_cat" : is_cat, "feed_id": feed_id, "show_excerpt": 1,"excerpt_length": 300, "show_content": 1,"include_attachments": 1, "since_id": since_id, "skip": skip, "limit": 20 }, 0, callback)
}

/**
 * Requests JSON-encoded article object with specific ID as @see https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#getarticle .
 * @param {string} article_ids -(comma-separated list of integers) - article IDs to operate on
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getArticle(article_ids, callback) {
    callApi("getArticle",{article_ids: article_ids}, 0, callback)
}

/**
 * Subscribes to specified feed, returns a status code. See subscribe_to_feed() in functions.php for details. @see https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#subscribetofeed-api-level-5-version-1-7-6 .
 * @param {string} feed_url - Feed URL (string)
 * @param {int} category_id - Category id to place feed into (defaults to 0, Uncategorized) (int)
 * @param {string} feed_login_password - feed credentials, self explanatory
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function subscribeToFeed(feed_url, category_id, feed_login_password, callback) {
    callApi("subscribeToFeed",{feed_url: feed_url, category_id:category_id, "login,password": feed_login_password},0 , callback)
}

/**
 * Unsubscribes specified feed. @see https://git.tt-rss.org/fox/tt-rss/wiki/ApiReference#unsubscribefeed-api-level-5-version-1-7-6 .
 * @param {int} feed_id - Feed id to unsubscribe from
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function unsubscribeFeed(feed_id, callback) {
    callApi("unsubscribeFeed",{feed_id: feed_id},0 ,callback)
}

/**
 * Tries to catchup (e.g. mark as read) specified feed.
 * @param {int} feed_id - ID of feed to update
 * @param {boolean} is_cat - True if the specified feed_id is a category
 * @param {string} mode - optional: one of all, 1day, 1week, 2week. defaults to all.
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function catchupFeed(feed_id, is_cat, mode, callback) {
    callApi("catchupFeed",{feed_id: feed_id, mode: mode, is_cat: is_cat}, 0, callback)
}

/**
 * Returns JSON-encoded list of categories with unread counts.
 *
 * Nested mode in this case means that a flat list of only topmost categories is returned and unread counters include counters for child categories.
 * This should be used as a starting point, to display a root list of all (for backwards compatibility) or topmost categories, use getFeeds to traverse deeper.
 *
 * @param {boolean} unread_only (bool) - only return categories which have unread articles
 * @param {boolean} enable_nested (bool) - switch to nested mode, only returns topmost categories
 * @param {string} include_empty (bool) - include empty categories.
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getCategories(unread_only, enable_nested, include_empty, callback) {
    callApi("getCategories",{unread_only: unread_only, enable_nested: enable_nested, include_empty: include_empty}, 0, callback)
}


/**
 * Returns full tree of categories and feeds.
 *
 * Note: counters for most feeds are not returned with this call for performance reasons.
 *
 * @param {boolean} include_empty (bool) - include empty categories
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getFeedTree(include_empty, callback) {
    callApi("getFeedTree",{include_empty: include_empty}, 0, callback)
}

/**
 * Search in TTRSS.
 * @param {string} search (string) - search query (e.g. a list of keywords)
 * @param {string} search_mode (string) - all_feeds, this_feed (default), this_cat (category containing requested feed)
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function search(search, search_mode, callback){
    callApi("getHeadlines",{feed_id: feed_id, search: search, search_mode: search_mode}, 0, callback)
}

/**
 * Returns JSON-encoded counter information.
 *
 * @param {string} (default: flc) - what kind of information to return (f - feeds, l - labels, c - categories, t - tags)
 * @param {function} callback - A callback function with parameters boolean (indicating
 *     success) and string (an optional error message).
 */
function getCounters(output_mode, callback) {
    callApi("getCounters",{output_mode: output_mode}, 0, callback)
}
/**
 * Gets the first image in body.
 * @param {string} html - The body
 *
 * @return The first image url or undefined elsewhere.
 */
function firstImg(html) {
    var m
    var urls = []
    //var imgRegex = /<img[^>]+src="(http:\/\/.*?[jpg|jpeg|gif|png][^">]+)"/g;
    //var imgRegex = /<img[^>]+src="(.*?[jpg|jpeg|gif|png][^"]+)"/g;
    var imgRegex = /<p.*?<img\b[^>]+?src\s*=\s*['"]?([^\s'"?#>]+)/g;
    var imgs=imgRegex.exec(html)

    /*
    If we want to iterate looking for more images
    while ( m = imgRegex.exec( html ) ) {
        urls.push( m[1] );
    }
    for (var i in imgs){
        console.debug("[ttrssJS]--found image--"+i+"->"+imgs[i])
        }
    */
    if (imgs){
      return imgs.length > 0 ? imgs[1]:undefined
    }else{
      return undefined
    }
}
