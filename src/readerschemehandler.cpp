#include "readerschemehandler.h"
#include <QFile>
#include <QBuffer>

#include <QDebug>
#include <QMap>
#include <QWebEngineUrlRequestJob>
#include <QWebEngineUrlScheme>
#include <QUrlQuery>
#include <QJsonObject>
 #include <QJsonDocument>

#define SCHEMENAME "reader"

const QByteArray ReaderSchemeHandler::schemeName = QByteArrayLiteral(SCHEMENAME);
// to render a readability parsed content
const QUrl ReaderSchemeHandler::readerUrl = QUrl(QStringLiteral(SCHEMENAME ":read"));
// to render a tiny tiny rss already parsed content
const QUrl ReaderSchemeHandler::articleUrl = QUrl(QStringLiteral(SCHEMENAME ":article"));
const QUrl ReaderSchemeHandler::quitUrl = QUrl(QStringLiteral(SCHEMENAME ":quit"));

ReaderSchemeHandler::ReaderSchemeHandler(QObject *parent) : QWebEngineUrlSchemeHandler(parent)
{

}

void ReaderSchemeHandler::registerUrlScheme()
{
    QWebEngineUrlScheme readerScheme(schemeName);
    readerScheme.setFlags(QWebEngineUrlScheme::SecureScheme |
                         QWebEngineUrlScheme::LocalScheme |
                         QWebEngineUrlScheme::LocalAccessAllowed);
    QWebEngineUrlScheme::registerScheme(readerScheme);
}

void ReaderSchemeHandler::requestStarted(QWebEngineUrlRequestJob *job)
{
    static const QUrl readerOrigin(QStringLiteral(SCHEMENAME ":"));
    static const QByteArray GET(QByteArrayLiteral("GET"));
    static const QByteArray POST(QByteArrayLiteral("POST"));

    QByteArray method = job->requestMethod();
    QUrl url = job->requestUrl();
    QUrl initiator = job->initiator();
    QUrlQuery query = QUrlQuery(url.query());
    QMap<QByteArray, QByteArray> requestHeaders = job->requestHeaders();

    if (method == GET && url == quitUrl ) { //&& initiator == pocketOrigin) {
        job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        //job->fail(QWebEngineUrlRequestJob::RequestAborted);
    } else if ( url == quitUrl ) { //&& initiator == pocketOrigin) {
        //job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        //job->fail(QWebEngineUrlRequestJob::RequestAborted);
        job->redirect(url.query());
        qDebug()<< url.query();

    }  else if ((url.path() == readerUrl.path() || url.path() == articleUrl.path() ) && method == GET) {
        //job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        // STATIC HTML
        /*
        QFile *file = new QFile(QStringLiteral(":/pages/reader.html"), job);
        file->open(QIODevice::ReadOnly);
        job->reply(QByteArrayLiteral("text/html"), file);
        qDebug()<< "READER!!!";
        */
        //DYNAMIC from GET // TODO study if cutelee
        //job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        QBuffer *buffer = new QBuffer;
        connect(job, SIGNAL(destroyed()), buffer, SLOT(deleteLater()));

        QFile *file = new QFile(QStringLiteral(":/pages/reader.html"), job);
        file->open(QIODevice::ReadOnly);
        htmlTemplate = file->readAll();
        QString articleScript;
        QJsonObject article;

        for (QPair<QString, QString> pair : query.queryItems()){
            qDebug() << pair.first << ":" << pair.second ;
            article.insert( pair.first, QJsonValue(pair.second));
        }

        QJsonDocument doc(article);

        // provide extension of the readability or ttrss article object from get parameters
        articleScript = "\n<script>var article_ext = %1 "
                        //"\nsetContent(article)"
                        "</script>";
        htmlTemplate.append(articleScript.arg(doc.toJson(QJsonDocument::Compact).toStdString().c_str()));
        buffer->open(QIODevice::WriteOnly);
        buffer->write(htmlTemplate.toUtf8());
        buffer->close();
        job->reply(QByteArrayLiteral("text/html"), buffer);

    } else {
        job->fail(QWebEngineUrlRequestJob::UrlNotFound);
    }
}
