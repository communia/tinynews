#ifndef POCKETSCHEMEHANDLER_H
#define POCKETSCHEMEHANDLER_H

#include <QObject>
#include <QWebEngineUrlSchemeHandler>


class PocketSchemeHandler : public QWebEngineUrlSchemeHandler
{
    Q_OBJECT
public:
    explicit PocketSchemeHandler(QObject *parent = nullptr );
    void requestStarted(QWebEngineUrlRequestJob *request) override;
    static void registerUrlScheme();
    const static QByteArray schemeName;
    const static QUrl aboutUrl;
    const static QUrl authorizedUrl;
    const static QUrl loggedUrl;
    const static QUrl quitUrl;
};


#endif // POCKETSCHEMEHANDLER_H
