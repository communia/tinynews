#include "pocketschemehandler.h"
#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QMap>
#include <QWebEngineUrlRequestJob>
#include <QWebEngineUrlScheme>

#define SCHEMENAME "pocket"

const QByteArray PocketSchemeHandler::schemeName = QByteArrayLiteral(SCHEMENAME);
const QUrl PocketSchemeHandler::aboutUrl = QUrl(QStringLiteral(SCHEMENAME "://about"));
const QUrl PocketSchemeHandler::loggedUrl = QUrl(QStringLiteral(SCHEMENAME "://logged"));
const QUrl PocketSchemeHandler::authorizedUrl = QUrl(QStringLiteral(SCHEMENAME "://authorized"));
const QUrl PocketSchemeHandler::quitUrl = QUrl(QStringLiteral(SCHEMENAME "://quit"));


PocketSchemeHandler::PocketSchemeHandler(QObject *parent) : QWebEngineUrlSchemeHandler(parent)
{

}

void PocketSchemeHandler::registerUrlScheme()
{
    QWebEngineUrlScheme pocketScheme(schemeName);
    pocketScheme.setFlags(QWebEngineUrlScheme::SecureScheme |
                         QWebEngineUrlScheme::LocalScheme |
                         QWebEngineUrlScheme::LocalAccessAllowed);
    QWebEngineUrlScheme::registerScheme(pocketScheme);
}
void PocketSchemeHandler::requestStarted(QWebEngineUrlRequestJob *job)
{
    static const QUrl pocketOrigin(QStringLiteral(SCHEMENAME ":"));
    static const QByteArray GET(QByteArrayLiteral("GET"));
    static const QByteArray POST(QByteArrayLiteral("POST"));

    QByteArray method = job->requestMethod();
    QUrl url = job->requestUrl();
    QUrl initiator = job->initiator();
    QMap<QByteArray, QByteArray> requestHeaders = job->requestHeaders();

    if (method == GET && url == aboutUrl) {
        QFile *file = new QFile(QStringLiteral(":/pages/about.html"), job);
        file->open(QIODevice::ReadOnly);
        job->reply(QByteArrayLiteral("text/html"), file);
    } else if (method == GET && url == loggedUrl) {
        QFile *file = new QFile(QStringLiteral(":/pages/pocket_logged.html"), job);
        file->open(QIODevice::ReadOnly);
        job->reply(QByteArrayLiteral("text/html"), file);
    } else if (method == GET && url == authorizedUrl) {
        QFile *file = new QFile(QStringLiteral(":/pages/oauth_authorized.html"), job);
        file->open(QIODevice::ReadOnly);
        job->reply(QByteArrayLiteral("text/html"), file);
    } else if (method == GET && url == quitUrl ) { //&& initiator == pocketOrigin) {
        job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        //job->fail(QWebEngineUrlRequestJob::RequestAborted);
    } else if (method == POST && url == quitUrl ) { //&& initiator == pocketOrigin) {
        job->fail(QWebEngineUrlRequestJob::UrlNotFound);
        //job->fail(QWebEngineUrlRequestJob::RequestAborted);
    } else {
        job->fail(QWebEngineUrlRequestJob::UrlNotFound);
    }
}
