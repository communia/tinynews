#ifndef READERSCHEMEHANDLER_H
#define READERSCHEMEHANDLER_H

#include <QWebEngineUrlSchemeHandler>
#include <QObject>

class ReaderSchemeHandler : public QWebEngineUrlSchemeHandler
{
    Q_OBJECT
public:
    explicit ReaderSchemeHandler(QObject *parent = nullptr);
    void requestStarted(QWebEngineUrlRequestJob *request) override;
    static void registerUrlScheme();
    QString htmlTemplate;
    const static QByteArray schemeName;
    const static QUrl readerUrl;
    const static QUrl articleUrl;
    const static QUrl quitUrl;
};

#endif // READERSCHEMEHANDLER_H
