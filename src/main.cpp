/*
    SPDX-License-Identifier: GPL-3.0-only
    SPDX-FileCopyrightText: 2021 Aleix Quintana Alsius <kinta@communia.org>
*/

#include "settingsmanager.h"
#include "tinynews-version.h"
#include <KDBusService>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickWebEngineProfile>
#include <QSortFilterProxyModel>
#include <QUrl>
#include <QWebEngineUrlRequestInterceptor>
#include <QWindow>
#include <QtQml>

#include <KAboutData>

#include "pocketschemehandler.h"
#include "readerschemehandler.h"

class PocketRequestInterceptor : public QWebEngineUrlRequestInterceptor
{
    Q_OBJECT

public:
    explicit PocketRequestInterceptor(QObject *parent = nullptr)
        : QWebEngineUrlRequestInterceptor(parent)
    {
    }

    void interceptRequest(QWebEngineUrlRequestInfo &info) override
    {
        Q_UNUSED(info)
        //qDebug() << info.requestUrl() ;
    }
};


// TODO complete it:
class OurSortFilterProxyModel : public QSortFilterProxyModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
public:
    void classBegin() override
    {
    }
    void componentComplete() override
    {
        if (dynamicSortFilter())
            sort(0);
    }
};

#include "main.moc" // TODO mv to dedicated header

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("tinyNews");
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("tinynews")));

    KAboutData about(QStringLiteral("tinyNews"),
                     i18n("Tint News"),
                     QStringLiteral(TINYNEWS_VERSION_STRING),
                     i18n("A Tiny tiny rss aggregator client"),
                     KAboutLicense::GPL,
                     i18n("© 2021 Communia"));
    about.addAuthor(i18n("Aleix Quintana Alsius"),
                    i18n("Lead developer"),
                    QStringLiteral("kinta@communia.org"),
                    QStringLiteral("https://planet.communia.org/users/kinta"),
                    QStringLiteral("kinta"));
    about.setBugAddress("https://gitlab.com/communia/tinynews/-/issues/new");
    about.setProductName("tinynews");
    about.setProgramLogo(QVariant(QIcon(QStringLiteral(":/images/tinynews.svg"))));
    about.setTranslator(i18ndc(nullptr, "NAME OF TRANSLATORS", "Your names"), i18ndc(nullptr, "EMAIL OF TRANSLATORS", "Your emails"));
    KAboutData::setApplicationData(about);

    QQmlApplicationEngine engine;

    QQuickWebEngineProfile* defaultProfile = QQuickWebEngineProfile::defaultProfile();
    defaultProfile->setPersistentCookiesPolicy(QQuickWebEngineProfile::ForcePersistentCookies);
    // To block some urls
    auto webEngineUrlInterceptor = new PocketRequestInterceptor();
    defaultProfile->setUrlRequestInterceptor(webEngineUrlInterceptor);
    //defaultProfile->setCachePath("profile");
    //defaultProfile->setPersistentStoragePath("profile");
    //To install custom Schemes
    auto pocketSchemeHandler = new PocketSchemeHandler();
    defaultProfile->installUrlSchemeHandler("pocket", pocketSchemeHandler);

    //To install custom Schemes
    auto readerSchemeHandler = new ReaderSchemeHandler();
    defaultProfile->installUrlSchemeHandler("reader", readerSchemeHandler);


    qmlRegisterAnonymousType<KAboutData>("org.kde.tinyNews.app", 1);
    qmlRegisterAnonymousType<KAboutLicense>("org.kde.tinyNews.app", 1);
    qmlRegisterAnonymousType<KAboutPerson>("org.kde.tinyNews.app", 1);
    qmlRegisterType<OurSortFilterProxyModel>("org.kde.tinyNews.app", 1, 0, "QSortFilterProxyModel");

    qmlRegisterSingletonInstance("org.kde.tinyNews", 1, 0, "SettingsManager", SettingsManager::self());
    qmlRegisterSingletonInstance("org.kde.tinyNews", 1, 0, "TinyProfile", defaultProfile);

    // Make sure that settings are saved before the application exits
    QObject::connect(&app, &QCoreApplication::aboutToQuit, SettingsManager::self(), &SettingsManager::save);

    engine.rootContext()->setContextProperty(QStringLiteral("tinyNewsAboutData"), QVariant::fromValue(KAboutData::applicationData()));
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    // One instance and prepare for dbus
    KDBusService service(KDBusService::Unique);

    return app.exec();
}
